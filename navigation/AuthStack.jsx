import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack'

import OnboardingScreen from '../screens/Auth/OnboardingScreen'
import LoginScreen from '../screens/Auth/LoginScreen'
import RegisterScreen from '../screens/Auth/RegisterScreen'
import ValidationScreens from '../screens/Auth/ValidationScreen'

const Stack = createNativeStackNavigator();

export const AuthStack = () => {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Onboarding" component={OnboardingScreen} />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="Validation" component={ValidationScreens} />
        </Stack.Navigator>
    );
};