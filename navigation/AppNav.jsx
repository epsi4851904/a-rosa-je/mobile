import React, {useContext} from "react";

import {NavigationContainer, DefaultTheme, DarkTheme,} from '@react-navigation/native';
import { AppStack } from "./AppStack";
import { AuthStack } from "./AuthStack";
import { AuthContext } from "../context/AuthContext";
import { ActivityIndicator, View, useColorScheme } from "react-native"

export const AppNav = () => {
    const {isLoading, userToken, userInfo} = useContext(AuthContext);
    const colorScheme = useColorScheme()

    if (isLoading) {
        return (
            <View style={[{flex:1, justifyContent: 'center', alignItems: 'center'}, {backgroundColor: colorScheme == 'dark' ? 'black' : 'white'}]}>
                <ActivityIndicator color="#66937B" size={'large'}/>
            </View>
        )
    }

    return (
        <NavigationContainer>
             { userToken && userInfo ? <AppStack /> : <AuthStack /> }
        </NavigationContainer>
    )
}