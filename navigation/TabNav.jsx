import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MessageScreen from '../screens/App/MessageScreen'
import DashboardScreen from '../screens/App/DashboardScreen'
import ProfilScreen from '../screens/App/ProfilScreen'
import AddPost from '../screens/App/AddPost'
import Ionicons from '@expo/vector-icons/Ionicons'
import MapScreen from '../screens/App/MapScreen';
import { Platform, View } from 'react-native';

const Tab = createBottomTabNavigator();

export const TabNavigator = () => {
    return (
      <Tab.Navigator screenOptions={{
        tabBarStyle: {
          position: 'absolute',
          bottom: 15,
          left: 5,
          right: 5,
          backgroundColor: '#66937B',
          elevation: 0,
          height: '10%',
          borderRadius: 100,
          height: 50
        }
      }}>
        <Tab.Screen
          name="Dashboard"
          component={DashboardScreen}
          options={{ 
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({focused}) => (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: Platform.OS == 'ios' ? 10 : '' }}>
                <Ionicons name={focused ? "home" : "home-outline"} color='white' size={25} />
              </View>
            )
            
          }}
        />
          <Tab.Screen
          name="Add Post"
          component={AddPost}
          options={{ 
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({focused}) => (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: Platform.OS == 'ios' ? 10 : '' }}>
                <Ionicons name={focused ? "add-circle" : "add-circle-outline"} color='white' size={25} />
              </View>
            )
          }}
        />
        <Tab.Screen
          name="MapScreen"
          component={MapScreen}
          options={{ 
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({focused}) => (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: Platform.OS == 'ios' ? 10 : '' }}>
                <Ionicons name={focused ? "map" : "map-outline"} color='white' size={25} />
              </View>
            )
          }}
        />
        <Tab.Screen
          name="Message"
          component={MessageScreen}
          options={{ 
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({focused}) => (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: Platform.OS == 'ios' ? 10 : '' }}>
                <Ionicons name={focused ? "chatbubbles" : "chatbubbles-outline"} color='white' size={25} />
              </View>
            )
          }}
        />
        <Tab.Screen
          name="Profil"
          component={ProfilScreen}
          options={{ 
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({focused}) => (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: Platform.OS == 'ios' ? 10 : '' }}>
                <Ionicons name={focused ? "person-circle" : "person-circle-outline"} color='white' size={25} />
              </View>
            )
          }}
        />
      </Tab.Navigator>
    );
  };
  