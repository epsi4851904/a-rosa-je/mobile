
import { createStackNavigator } from '@react-navigation/stack'
import { TabNavigator } from './TabNav'
import MessageDetailScreen from '../screens/App/Message/MessageDetailScreen'
import CustomHeader from '../components/message/CustomHeader'
import SettingsModal from '../screens/App/profil/SettingsModal'
import UserPage from '../screens/App/profil/UserPageScreen'
import DetailPost from '../screens/App/post/PostDetail'
import UpdatePostModal from '../components/post/UpdatePostModal'
import CreateCommentModal from '../components/comment/CreateCommentModal'
import ReadCommentModal from '../components/comment/ReadCommentModal'
import AddImage from '../components/post/AddImage'
import MapScreen from '../screens/App/MapScreen'
import AutorisationScreen from '../screens/App/Authorisation/AutorisationScreen'
import { useContext } from 'react'
import { AuthContext } from '../context/AuthContext'
import UpdatePost from '../screens/App/post/UpdatePost'
import { useColorScheme } from 'react-native'

const Stack = createStackNavigator();

export const AppStack = () => {
    const theme = useColorScheme()
    return (
        <Stack.Navigator theme={{dark: true}}>
            <Stack.Screen name="Main" component={TabNavigator} options={{ headerShown: false}} />
            <Stack.Screen name="message_detail" component={MessageDetailScreen} options={
                ({route}) => ({
                    headerShown: true,
                    headerBackTitleVisible: false,
                    cardStyle: {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5'},
                    headerStyle: {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5', borderBottomColor: '#80808080', borderBottomWidth: 1, height: 100},
                    headerTintColor: theme == 'dark' ? 'white' : 'black'
                })
            } />
            <Stack.Screen name="settings_modal" component={SettingsModal} options={
                {
                    presentation: 'modal',
                    title: '',
                    headerBackTitleVisible: false,
                    headerLeft: () => <CustomHeader />,
                    cardStyle: {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5'},
                    headerStyle: {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5'},
                    headerTintColor: theme == 'dark' ? 'white' : 'black'
                }
            } />
            <Stack.Screen name="user_detail" component={UserPage} options={
                {
                    headerShown: false
                }
            } />
            <Stack.Screen name="post_detail" component={DetailPost} options={
                {
                    headerShown: false
                }
            } />
            <Stack.Screen name="update_post_modal" component={UpdatePostModal} options={
                {
                    presentation: 'modal',
                    title: 'Modification du post',
                    headerBackTitleVisible: false,
                }
            } />
            <Stack.Screen name="create_comment_modal" component={CreateCommentModal} options={
                {
                    presentation: 'modal',
                    title: '',
                    headerBackTitleVisible: false
                }
            } />
            <Stack.Screen name="read_comment_modal" component={ReadCommentModal} options={
                {
                    presentation: 'modal',
                    title: '',
                    headerBackTitleVisible: false
                }
            } />
            <Stack.Screen name="add_image" component={AddImage} options={
                {
                    presentation: 'modal',
                    title: 'Ajouter votre image',
                    headerBackTitleVisible: false,
                    cardStyle: {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5'},
                    headerStyle: {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5'},
                    headerTintColor: theme == 'dark' ? 'white' : 'black'
                }
            } />
            <Stack.Screen name="screen_map" component={MapScreen} options={
                {
                    presentation: 'modal',
                    title: '',
                    headerBackTitleVisible: false,
                }
            } />
            <Stack.Screen name="autorisation_screen" component={AutorisationScreen} options={
                {
                    headerShown: false,
                    title: '',
                    headerBackTitleVisible: false,
                }
            } />
            <Stack.Screen name="update_post" component={UpdatePost} options={
                {
                    headerShown: false,
                    title: '',
                    headerBackTitleVisible: false,
                }
            } />
        </Stack.Navigator>
    );
};