import React, { useContext, useState } from 'react'
import { View, Text, ImageBackground, TouchableOpacity, StyleSheet, Alert } from 'react-native'
import { APP_URL } from '../config'
import Ionicons from '@expo/vector-icons/Ionicons'
import Badge from './Badge'
import moment from 'moment'
import Modal from "react-native-modal"
import { AuthContext } from '../context/AuthContext'
import { useNavigation } from '@react-navigation/native'
import axios from 'axios'

const renderImageMain = (images) => {
  const image = images.filter(item => item.isMain === true)
  return image[0]
}

const Box = ({ post, detail, isProfil = false, url = '', update, userToken  }) => {
  const {userInfo} = useContext(AuthContext)
  const calculateDuration = () => {
    const now = moment();
    const start = moment(post.startAt);
    const end = moment(post.endAt)

    const diffStart = start.diff(now, 'days');
    const diffEnd = end.diff(now, 'days');

    if (diffStart < diffEnd && now.isBefore(start)) {
        return `Commence dans ${diffStart} jour(s)`
    } else if (now.isBetween(start, end, null, '[]')) {
        return diffEnd == 0 ? 'dernier jour' : `En cours (${diffEnd} jour(s) restant)`
    } else if (now.isAfter(end)) {
        return 'Terminé'
    } else {
        return `Commence dans ${diffEnd} jour(s)`
    }
  }
  const [isVisible, setIsVisible] = useState(false)

  const closeModal = () => {
    setIsVisible(false)
  }

  const openModal = () => {
    if (isProfil) {
      setIsVisible(true)
    }
  }

  const handleDelete = () => {
    const options = {
        method: 'DELETE',
        url: `${APP_URL}/api/post/${post.id}`,
        headers: {
            Authorization: `Bearer ${userToken}`
        }
    }
    axios.request(options).then((response) => {
        if (response.data.message == 'Post supprimé avec succès') {
          closeModal()
        }
    })
}

const deletePost = () => {
    Alert.alert(
        'Suppresion de cette belle plante',
        'Etes vous sur de vouloir la supprimer ? cette action est irréversible',
        [
            {
              text: 'Non',
              style: 'cancel',
            },
            {
              text: 'Oui',
              onPress: () => {
                handleDelete()
              },
            },
          ]
    );
}


  return (
    <TouchableOpacity onPress={detail} onLongPress={() => openModal()} className="w-1/2 h-72 max-w-sm rounded shadow-t-lg px-2">
      {post.keepedBy && post.keepedBy.id != null && !post.keepingValidate && post.postedBy.id == userInfo.id && (
        <Badge style={{paddingHorizontal: 8,paddingVertical: 5,borderRadius: 10,minWidth: 10,position: 'absolute',top: -5, right: 0, backgroundColor: '#FF000095', zIndex: 50}} text="!" />
      )}
      <View className="h-4/6">
        <ImageBackground
          source={{uri: APP_URL + url + renderImageMain(post.images).name }}
          resizeMode="cover"
          className="h-full rounded overflow-hidden"
        >
          {post.startAt != null && post.endAt != null && (
            <Badge text={calculateDuration()} style={{paddingHorizontal: 8,paddingVertical: 5,borderRadius: 10,marginTop: 5, marginRight: 5,minWidth: 10,position: 'absolute',right: 0, backgroundColor: '#66937B70'}} />
          )}
        </ImageBackground>
      </View>
      <View className="h-1/6">
        <View className="bg-transparent">
          <Text className="text-vert-v2 text-lg font-bold">{post.namePlant}</Text>
          <View className="flex-row items-center">
            <Ionicons name="location-sharp" color="#66937B" size={15}/>
            <Text className="text-vert-v2">{post.city} {post.zipcode}</Text>
          </View>
        </View>
      </View>
      <Modal
        isVisible={isVisible}
        onBackdropPress={closeModal}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        backdropOpacity={0.5}
        onSwipeMove={closeModal}
        style={{margin: 'none', borderTopLeftRadius: 10}}
      >
          <View
            style={{
              height: '25%',
              marginTop: 'auto',
              backgroundColor: '#101010',
              borderTopWidth: 0.5,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10
            }}
          >
            <View className="flex items-center justify-center">
              <Ionicons name="remove-outline" color="white" size={25}/>
            </View>
          <View>
          <View style={styles.container}>
            <TouchableOpacity style={styles.box} onPress={update}>
                <Text style={styles.boxTitle}>Modifier</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box} onPress={deletePost}>
                <Text style={[styles.boxTitle, {color: 'red', marginRight: '50px'}]}>Supprimer</Text>
                <Ionicons name="trash" color='red' size={15} />
            </TouchableOpacity>
          </View>
          </View>
        </View>
      </Modal>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ffffff20',
    borderBottomWidth: 1,
    padding: 20,
    marginBottom: 10
  },
  boxTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginRight: '50'
  },
})

export default Box;