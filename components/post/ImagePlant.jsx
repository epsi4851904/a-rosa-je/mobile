import React, { useContext, useState } from 'react';
import { View, Image, TouchableOpacity, Alert, StyleSheet, Text } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Modal from "react-native-modal";
import { APP_URL } from '../../config';
import { AuthContext } from '../../context/AuthContext';
import axios from 'axios';

const ImagePlant = ({ image, post, refreshPost }) => {
  const { userInfo, userToken } = useContext(AuthContext);
  const [isVisible, setIsVisible] = useState(false);

  const openModal = () => {
    if (userInfo.id === post.keepedBy.id) {
      setIsVisible(true);
    }
  };

  const closeModal = () => {
    setIsVisible(false);
  };

  const handleDelete = () => {
    const options = {
      method: 'DELETE',
      url: `${APP_URL}/api/post/${image.id}/delete-image`,
      headers: {
        Authorization: `Bearer ${userToken}`
      }
    };
    axios.request(options).then((response) => {
      console.log(response.data)
      if (response.data.message === 'Image deleted') {
        closeModal();
        refreshPost();
      }
    }).catch((error) => {
      console.error(error);
    });
  };

  const deleteImage = () => {
    Alert.alert(
      'Suppresion de cette image',
      'Etes vous sur de vouloir la supprimer ? cette action est irréversible',
      [
        {
          text: 'Non',
          style: 'cancel',
        },
        {
          text: 'Oui',
          onPress: () => {
            handleDelete();
          },
        },
      ]
    );
  };

  return (
    <View className="w-48 h-72 mx-2">
      <TouchableOpacity onLongPress={openModal}>
        <Image
          source={{ uri: APP_URL + image.name }}
          className="w-full h-full rounded-lg"
        />
      </TouchableOpacity>
      <Modal
        isVisible={isVisible}
        onBackdropPress={closeModal}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        backdropOpacity={0.5}
        onSwipeMove={closeModal}
        style={{ margin: 'none', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
      >
        <View
          style={{
            height: '25%',
            marginTop: 'auto',
            backgroundColor: '#101010',
            borderTopWidth: 0.5,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10
          }}
        >
          <View className="flex items-center justify-center">
            <Ionicons name="remove-outline" color="white" size={25} />
          </View>
          <View style={styles.container}>
            <TouchableOpacity style={styles.box} onPress={deleteImage}>
              <Text style={[styles.boxTitle, { color: 'red', marginRight: 10 }]}>Supprimer</Text>
              <Ionicons name="trash" color='red' size={15} />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ffffff20',
    borderBottomWidth: 1,
    padding: 20,
    marginBottom: 10
  },
  boxTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  container: {
    padding: 20,
  },
});

export default ImagePlant;
