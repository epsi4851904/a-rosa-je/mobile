import { StyleSheet, Text, View, Button, SafeAreaView, ScrollView, Image, TextInput, TouchableOpacity , Alert} from 'react-native';
import { APP_URL } from '../../config';
import React, { useState, useContext } from 'react';
import { AuthContext } from '../../context/AuthContext';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

export default function UpdatePostModal({route}) {
    const navigation = useNavigation()
    const {userToken, userInfo, setIsLoading} = useContext(AuthContext)
    const { post } = route.params;
    const [plantName, setPlantName] = useState(post.namePlant);
    const [description, setDescription] = useState(post.description);
    const [city, setCity] = useState(post.city);
    const [zipcode, setZipcode] = useState(post.zipcode);

    const updatePost = async () => {
        setIsLoading(true)
        const formData = new FormData();
        formData.append("namePlant", plantName);
        formData.append("description", description);
        formData.append("city", city);
        formData.append("zipcode", zipcode);        

        const options = {
            method: 'POST',
            url: `${APP_URL}/api/post/${post.id}`,
            headers: {
                "Content-Type": "multipart/form-data",
                Authorization: `Bearer ${userToken}`
            },
            data: formData
        }
        axios.request(options).then((response) => {
            return response.data
        }).catch((error) => {
            console.error(error)
        })
        setIsLoading(false)
        navigation.goBack()
    }

    const renderImageMain = (images) => {
        const image = images.filter(item => item.isMain === true)
        return image[0]
    }
    
    return (
        <SafeAreaView className="flex-1">
            <ScrollView className="p-5 w-full">
                <TextInput
                        className="input input-bordered w-full mb-5 p-2 border border-[#109A0D] text-[#034B01] rounded-lg text-white text-2xl"
                        value={plantName}
                        onChangeText={setPlantName}
                        placeholder="Nom plante"
                        placeholderTextColor="rgba(3, 75, 1, 0.68)"
                    />
                <View className="mx-auto mb-5 p-1 w-full h-72 justify-center items-center">
                    <Image
                        source={{ uri: APP_URL + renderImageMain(post.images).name }}
                        resizeMode="cover"
                        className="w-full h-full rounded"
                    />
                </View>
                <View className="w-full px-5">
                    
                    <TextInput
                        className="input input-bordered w-full mb-4 p-2 border border-[#109A0D] text-[#034B01] rounded-lg text-white"
                        placeholder="Ville"
                        placeholderTextColor="rgba(3, 75, 1, 0.68)"
                        value={city}
                        onChangeText={setCity}
                    />
                    <TextInput
                        className="input input-bordered w-full mb-4 p-2 border border-[#109A0D] text-[#034B01] rounded-lg text-white"
                        placeholder="Code postal"
                        placeholderTextColor="rgba(3, 75, 1, 0.68)"
                        value={zipcode}
                        onChangeText={setZipcode}
                    />
                    <TextInput
                        className="input input-bordered w-full p-2 border border-[#109A0D] text-[#034B01] rounded-lg"
                        placeholder="Description"
                        multiline={true} 
                        numberOfLines={4}
                        placeholderTextColor="rgba(3, 75, 1, 0.68)"
                        value={description}
                        onChangeText={setDescription}
                    />
                </View>
                <View className="mt-5 flex-row justify-center">
                    <TouchableOpacity className="btn bg-green-500 w-48 h-12 flex items-center justify-center" onPress={updatePost}>
                        <Text className="text-white text-lg">Modifier le post</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </SafeAreaView>

    )
}