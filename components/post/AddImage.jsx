import React, { useContext, useState } from "react";
import { View, Text, Button, Image, StyleSheet, Platform, ScrollView, TouchableOpacity, useColorScheme } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { APP_URL } from "../../config";
import { AuthContext } from "../../context/AuthContext"
import Ionicons from '@expo/vector-icons/Ionicons'
import { useFocusEffect, useNavigation, useRoute } from "@react-navigation/native";

export default function AddImage() {
    const [imageSource, setImageSource] = useState(null)
    const {userToken, setIsLoading} = useContext(AuthContext)
    const route = useRoute()
    const id = route.params.id
    const navigation = useNavigation()
    const colorScheme = useColorScheme()

    const pickImage = async () => {
        if (Platform.OS !== "web") {
            const { status } = await ImagePicker.requestCameraPermissionsAsync();
            if (status !== "granted") {
                alert("Permission denied to access camera roll");
                return;
            }
        }

        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            quality: 0.7,
        });

        if (result) {
            if (!result.cancelled && result.assets.length > 0 && result.assets[0].uri) {
                const imageType = result.assets[0].type || 'image/jpeg';
                await setImageSource({ uri: result.assets[0].uri, type: imageType });
            } else {
                console.error("Image selection was cancelled or URI is undefined.");
            }
        } else {
            console.error("Error occurred during image selection.");
        }
    }

    const handleRefresh = async () => {
        await pickImage()
    }

    useFocusEffect(
        React.useCallback(() => {
            handleRefresh()
        }, [])
    )

    const sendImage = async () => {
        if (!imageSource) {
            Alert.alert(
                'Erreur',
                'Certains champs ne sont pas remplis',
                [
                    { text: 'OK' },
                ],
                { cancelable: false }
            );
            return;
        }
        const formData = new FormData()
        const file = {
            uri: imageSource.uri,
            name: 'photo.jpg',
            type: 'image/jpg'
        }
        formData.append("image", file)
        const response = await fetch(`${APP_URL}/api/post/${id}/add-image`, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${userToken}`,
            },
            body: formData
        })
        setImageSource(null)
        navigation.goBack()
    }

    return (
        <View className="w-full" style={{backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}}>
            {imageSource && (
                <View className="w-full h-96 flex items-center">
                    <Image source={imageSource} style={styles.image} className="mt-5" />
                    <View className="flex flex-row w-full items-center justify-around pt-5">
                        <TouchableOpacity style={styles.box} className="bg-[#808080] w-1/3 mt-5" onPress={() => pickImage()}>
                            <Text style={styles.boxTitle} className="ml-2">
                                Reprendre
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.box} className="bg-vert-v2 w-1/3 mt-5" onPress={() => sendImage()}>
                            <Text style={styles.boxTitle} className="ml-2">
                                Valider
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
}
const styles = StyleSheet.create({
    box: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        padding: 15,
        margin: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 2,
    },
    boxTitle: {
        fontSize: 15,
        textAlign: 'center',
        color: 'black'
    },
    image: {
        width: '90%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 4,
    }
});
