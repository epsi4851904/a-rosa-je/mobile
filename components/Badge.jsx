import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const Badge = ({ text, style }) => {
  return (
    <View style={style}>
      <Text style={styles.text}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});

export default Badge;
