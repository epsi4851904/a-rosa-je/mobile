import * as React from 'react';
import { ScrollView, Text, View, useWindowDimensions } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Box from '../Box';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import axios from 'axios';
import { APP_URL } from '../../config';
import { AuthContext } from '../../context/AuthContext';

const PostRoute = ({ posts }) => {
    const navigation = useNavigation()
    return (
        <ScrollView>
            <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                {posts.map((item) => (
                    <Box
                    key={item.id}
                    post={item}
                    detail={() => navigation.navigate('post_detail', {id: item.id})}
                    />
                ))}
            </View>
        </ScrollView>
    )
}

const KeepedRoute = ({ keeped }) => {
    const navigation = useNavigation()
    return (
        <ScrollView>
            <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                {keeped.map((item) => (
                    <Box
                    key={item.id}
                    post={item}
                    detail={() => navigation.navigate('post_detail', {id: item.id})}
                    />
                ))}
            </View>
        </ScrollView>
    )
}

const WaitingRoute = ({ waiting }) => {
    const navigation = useNavigation()
    return (
        <ScrollView>
            <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                {waiting.map((item) => (
                    <Box
                    key={item.id}
                    post={item}
                    detail={() => navigation.navigate('post_detail', {id: item.id})}
                    />
                ))}
            </View>
        </ScrollView>
    )
}

const renderScene = (routeProps) => {
    const { route } = routeProps
    switch (route.key) {
        case 'post':
            return <PostRoute posts={route.posts} />
        case 'keeped':
            return <KeepedRoute keeped={route.keeped} />
        case 'waiting':
            return <WaitingRoute waiting={route.waiting} />
        default:
            return null
    }
}


const renderTabBar = props => (
    <TabBar
        {...props}
        indicatorStyle={{ backgroundColor: 'white' }}
        style={{ backgroundColor: 'transparent', position: 'fixed' }}
        renderLabel={({ route, focused, color }) => (
            route.key === 'post' ? (
                <Ionicons name={focused ? 'leaf' : 'leaf-outline'} color={color} size={15} />
            ) : route.key === 'keeped' ? (
                <Ionicons name={focused ? 'water' : 'water-outline'} color={color} size={15} />
            ) : route.key === 'waiting' ? (
                <Ionicons name={focused ? 'hourglass' : 'hourglass-outline'} color={color} size={15} />
            ) : null
        )}
    />
)

export default function TabViewExample () {
  const layout = useWindowDimensions()
  const [posts, setPosts] = React.useState([])
  const [keeping, setKeeping] = React.useState([])
  const [waiting, setWaiting] = React.useState([])
  const {userInfo, userToken} = React.useContext(AuthContext)
  const [routes, setRoutes] = React.useState([])
  const [index, setIndex] = React.useState(0)

  const getUserPost = async () => {
    const options = {
      method: 'GET',
      url: `${APP_URL}/api/profil/posts/${userInfo.id}`,
      headers: {
        Authorization: `Bearer ${userToken}`
      }
    }
    await axios.request(options).then((response) => {
      setPosts(response.data.posts)
      setKeeping(response.data.keeping)
      setWaiting(response.data.waiting)
        setRoutes([
          { key: 'post', title: 'Mes posts', posts: response.data.posts },
          { key: 'keeped', title: 'Mes gardiennages', keeped: response.data.keeping },
          { key: 'waiting', title: 'En attente', waiting: response.data.waiting }
        ])
        setIndex(0)
    }).catch((error) => {
      console.error(error)
    })
  }

  const handleRefresh = async () => {
    await getUserPost()
  }

  useFocusEffect(
      React.useCallback(() => {
          handleRefresh()
      }, [])
  )

  return (
    <TabView
        key={index}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        renderTabBar={renderTabBar}
        initialLayout={{ width: layout.width, height: (layout.height / 2) }}
        style={{height: layout.height}}
    />
  )
}
