import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

const RadioButton = ({ selected }) => {
  return (
    <View style={styles.radioButton}>
      {selected && <View style={styles.innerRadioButton} />}
    </View>
  );
};

const styles = StyleSheet.create({
  radioButton: {
    height: 24,
    width: 24,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: '#66937B',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerRadioButton: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: '#66937B',
  },
});

export default RadioButton;