import React from 'react';
import {View, Text, TouchableOpacity, TextInput, useColorScheme, StyleSheet} from 'react-native';

export default function InputField({
  label,
  icon,
  inputType,
  keyboardType,
  fieldButtonLabel,
  fieldButtonFunction,
  value,
  onChangeText
}) {
  const colorScheme = useColorScheme()
  return (
    <View 
      className="flex flex-row items-center justify-center rounded-full p-4 my-2 shadow shadow-black bg-[#ffffff]"
      style={[{backgroundColor: colorScheme === 'dark' ? '#333333' : '#ffffff'}, styles.shadow]}

    >
      {icon}
      {inputType == 'password' ? (
        <TextInput
          placeholder={label}
          placeholderTextColor="grey"
          keyboardType={keyboardType}
          style={{flex: 1, paddingVertical: 0, color: colorScheme === 'dark' ? 'white' : 'black'}}
          secureTextEntry={true}
          textColor="black"
          value={value}
          onChangeText={onChangeText}
          autoCapitalize="none"
        />
      ) : (
        <TextInput
          placeholder={label}
          placeholderTextColor="grey"
          keyboardType={keyboardType}
          style={{flex: 1, paddingVertical: 0,color: colorScheme === 'dark' ? 'white' : 'black'}}
          textColor="black"
          value={value}
          onChangeText={onChangeText}
          autoCapitalize="none"
          className="ml-2"
        />
      )}
      <TouchableOpacity onPress={fieldButtonFunction}>
        <Text style={{color: '#AD40AF', fontWeight: '700'}}>{fieldButtonLabel}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000', 
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
  }
})