import { useContext } from "react";
import { TouchableOpacity, View, Text, Image } from "react-native";
import { AuthContext } from "../../context/AuthContext";

const renderText = (conversation) => {
    const {userInfo} = useContext(AuthContext)
    const user = userInfo
    if (conversation.isSend) {
        return conversation.messages[0].text
    } else if (conversation.nbNewMessage > 0) {
        return `${conversation.nbNewMessage} nouveaux messages`
    } else if (conversation.last[0].recipient.id === user.id || conversation.last[0].viewAt === null) {
        return conversation.last[0].text
    } else {
        return 'vu'
    }
}

const BoxConversation = ({conversation, navigation, colorScheme}) => (
    <TouchableOpacity style={{ width: '100%', padding: 10 }} onPress={
        () => navigation.navigate('message_detail', {
            id: conversation.user.id,
            firstName: conversation.user.firstName,
            lastName: conversation.user.lastName
        })
        }>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image 
                source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ conversation.user.firstName + conversation.user.lastName +'&backgroundColor[]' }} 
                style={{ width: 50, height: 50, borderRadius: 25, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
                className="border-1"
            />
            <View style={{ flex: 1 }}>
                <Text style={{ fontWeight: 'bold', color: colorScheme == 'dark' ? '#F5F5F5' : 'black' }}>{conversation.user.firstName} {conversation.user.lastName}</Text>
                <Text style={{ color: '#aaa' }}>
                    {renderText(conversation)}
                </Text>
            </View>
        </View>
    </TouchableOpacity>
)

export default BoxConversation;