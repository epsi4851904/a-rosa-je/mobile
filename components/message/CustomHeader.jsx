
import React from 'react';
import { Text, TouchableOpacity, useColorScheme } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons'
import { useNavigation } from '@react-navigation/native';

const CustomHeader = ({id}) => {
  const navigation = useNavigation()
  const colorScheme = useColorScheme()
  return (
    <TouchableOpacity className="mr-5" onPress={() => navigation.navigate('user_detail', {id})}>
      <Ionicons name="close-outline" color={colorScheme == 'dark' ? 'white' : 'black'} size={30} />
    </TouchableOpacity>
  );
};

export default CustomHeader;
