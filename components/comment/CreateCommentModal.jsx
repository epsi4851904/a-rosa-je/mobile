import React, { useState, useContext } from "react";
import { View, TextInput, Button, Alert, StyleSheet  } from "react-native";
import axios from 'axios';
import { APP_URL } from "../../config";
import { AuthContext } from "../../context/AuthContext";
import { useRoute } from "@react-navigation/native";

export default function CreateCommentModal() {
    const {userToken, userInfo} = useContext(AuthContext)
    const [newComment, setNewComment] = useState('');
    const route = useRoute()
    const id = route.params.id

    const sendComment = async () => {
        try {
            if (newComment.trim() !== '') {
                const formData = new FormData();
                formData.append('comment', newComment);

                const options = {
                    method: 'POST',
                    url: `${APP_URL}/api/comment/${id}`,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'User-Agent': 'Insomnia/2023.5.7',
                        Authorization: `Bearer ${userToken}`
                    },
                    data: formData
                };

                const response = await axios.request(options);
                setNewComment('');
            } else {
                Alert.alert('Error', 'Please enter a non-empty comment');
            }
        } catch (error) {
            console.error('Error sending comment:', error);
            Alert.alert('Error', 'Failed to send comment. Please try again later.');
        }
    };

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                placeholder="Entrer votre commentaire ici"
                value={newComment}
                onChangeText={text => setNewComment(text)}
            />
            <Button
                title="Envoyer  "
                onPress={sendComment}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center', 
        paddingHorizontal: 20,
    },
    input: {
        borderWidth: 1,
        borderColor: 'gray',
        padding: 10,
        marginBottom: 20,
        width: '100%',
    },
});
