import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';

const CommentSection = ({ comments }) => {
    return (
        <TouchableOpacity style={{ width: '100%', padding: 10 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image 
                    source={{ uri: 'https://www.example.com/default-avatar.jpg' }} 
                    style={{ width: 50, height: 50, borderRadius: 25, marginRight: 10 }}
                />
                <View style={{ flex: 1 }}>
                    <Text style={{ fontWeight: 'bold' }}>{comments[0].postedBy.firstName} {comments[0].postedBy.lastName}</Text>
                    <Text style={{ color: comments.isSend && comment.nbNewMessage > 0 ? '#aaa' : 'blue' }}>{comments[0].text}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default CommentSection;
