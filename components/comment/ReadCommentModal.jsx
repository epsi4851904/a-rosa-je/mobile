import React from "react";
import { Text, View, FlatList, Image, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function ReadCommentModal({ route }) {
    const { comments } = route.params;
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <FlatList
                data={comments}
                keyExtractor={item => item.id.toString()}
                renderItem={({ item }) => (
                    <View style={styles.commentContainer}>
                        <TouchableOpacity onPress={() => navigation.navigate('user_detail', {id: item.postedBy.id})}>
                            <Image 
                                source={{ uri: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Ficonduck.com%2Ficons%2F255392%2Fuser-avatar-happy&psig=AOvVaw3oemda4j3ma8y7R4Vgj6ku&ust=1708528530067000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCKjKifOauoQDFQAAAAAdAAAAABAE' }} 
                                style={styles.avatar}
                            />
                        </TouchableOpacity>
                        <View style={{block:1}}>
                            <Text style={{marginLeft: 5}}>{item.postedBy.firstName} {item.postedBy.lastName}</Text>
                            <View style={styles.commentBubble}>
                                <Text style={{color: 'white'}}>{item.text}</Text>
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
    },
    commentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    avatar: {
        width: 25,
        height: 25,
        borderRadius: 25,
        marginRight: 10,
        backgroundColor: '#00cc00',
    },
    commentBubble: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#00cc00',

    },
});
