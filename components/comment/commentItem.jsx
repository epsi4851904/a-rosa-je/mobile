import React, { useState, useContext } from 'react';
import { View, Text, TouchableOpacity, Alert, StyleSheet, TextInput, Image, useColorScheme } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Modal from 'react-native-modal';
import axios from 'axios';
import { APP_URL } from '../../config';
import { AuthContext } from '../../context/AuthContext';

const CommentItem = ({ comment, userToken, refreshPost, navigation }) => {
  const { userInfo } = useContext(AuthContext);
  const [isVisible, setIsVisible] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [editedComment, setEditedComment] = useState(comment.text);
  const colorScheme = useColorScheme();

  const openModal = () => {
    if (userInfo.id === comment.postedBy.id) {
      setIsVisible(true);
    }
  };

  const closeModal = () => {
    setIsVisible(false);
    setIsEditing(false);
  };

  const handleDelete = () => {
    const options = {
      method: 'DELETE',
      url: `${APP_URL}/api/comment/${comment.id}`,
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };
    axios
      .request(options)
      .then((response) => {
        if (response.data.message === 'Comment deleted') {
          closeModal();
          refreshPost();
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const deleteComment = () => {
    Alert.alert(
      'Suppression de ce commentaire',
      'Êtes-vous sûr de vouloir le supprimer ? Cette action est irréversible.',
      [
        {
          text: 'Non',
          style: 'cancel',
        },
        {
          text: 'Oui',
          onPress: () => {
            handleDelete();
          },
        },
      ]
    );
  };

  const handleEdit = () => {
    if (editedComment.trim() !== '') {
      const formData = new FormData();
      formData.append('comment', editedComment);

      const options = {
        method: 'POST',
        url: `${APP_URL}/api/comment/${comment.id}/update`,
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${userToken}`
        },
        data: formData
      };
      axios
        .request(options)
        .then((response) => {
          if (response.data.message === 'Commentaire modifié') {
            setIsEditing(false)
            refreshPost();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    };
  }
  return (
    <TouchableOpacity onLongPress={openModal}>
      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: 20 }}>
        <TouchableOpacity onPress={() => navigation.navigate('user_detail', { id: comment.postedBy.id })}>
          <Image
            source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed=' + comment.postedBy.firstName + comment.postedBy.lastName + '&backgroundColor[]' }}
            style={{ width: 40, height: 40, borderRadius: 25, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
          />
        </TouchableOpacity>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          {isEditing ? (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TextInput
                style={[styles.textInput, { flex: 1 }]}
                value={editedComment}
                onChangeText={setEditedComment}
                placeholder="Modifier le commentaire"
                placeholderTextColor="#ccc"
              />
              <TouchableOpacity style={styles.saveButton} onPress={handleEdit}>
                <Ionicons name="checkmark-circle" color="green" size={25} />
              </TouchableOpacity>
            </View>
          ) : (
            <>
              <Text style={{ fontWeight: 'bold', color: colorScheme == 'dark' ? '#F5F5F5' : '#333333' }}>
                {comment.postedBy.firstName} {comment.postedBy.lastName}
                <Text style={{ fontWeight: 'normal' }}>
                  {userInfo.id == comment.postedBy.id ? ' (vous)' : ''}
                </Text>
              </Text>
              <Text style={{ color: colorScheme == 'dark' ? '#F5F5F5' : '#333333' }}>{comment.text}</Text>
            </>
          )}
        </View>
      </View>
      <Modal
        isVisible={isVisible} 
        onBackdropPress={closeModal} 
        animationIn="slideInUp" 
        animationOut="slideOutDown" 
        backdropOpacity={0.5}
         style={{ margin: 0, borderTopLeftRadius: 10 }}>
        <View style={{ height: '25%', marginTop: 'auto', backgroundColor: '#101010', borderTopWidth: 0.5, borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
          <View style={styles.modalHeader}>
            <Ionicons name="remove-outline" color="white" size={25} />
          </View>
          <View style={styles.container}>
            {(
              <>
                <TouchableOpacity style={styles.box} onPress={() => {closeModal();setIsEditing(true);}}>
                  <Text style={styles.boxTitle}>Modifier</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.box} onPress={deleteComment}>
                  <Text style={[styles.boxTitle, { color: 'red', marginRight: 10 }]}>Supprimer</Text>
                  <Ionicons name="trash" color="red" size={15} />
                </TouchableOpacity>
              </>
            )}
          </View>
        </View>
      </Modal>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ffffff20',
    borderBottomWidth: 1,
    padding: 20,
    marginBottom: 10,
  },
  boxTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginRight: '50'
  },
  textInput: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
  },
  saveButton: {
    marginLeft: 10,
    justifyContent: 'center',
  },
  modalHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
});

export default CommentItem;
