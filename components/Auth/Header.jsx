import React from 'react'
import { View, Text, StatusBar, TouchableOpacity} from 'react-native'
import { Image } from 'expo-image'
import Ionicons from '@expo/vector-icons/Ionicons'

const Header = ({ navigation, isHome, colorScheme, title = 'Arosaje' }) => (
    <View className="items-center relative">
        <View className="flex flex-row justify-center items-center w-full">
            {!isHome && (
                <TouchableOpacity className="absolute left-5" onPress={() => navigation.goBack()}>
                    <Ionicons name="chevron-back-outline" color='#66937B' size={30} />
                </TouchableOpacity>
            )}
            <View className="flex-row items-center justify-center h-12">
                {!isHome && (
                    <Image
                        source={require('../../assets/logoV2.png')}
                        className="w-12 h-full"
                    />
                )}
            <Text className='text-gray-400 font-bold uppercase text-xl ${textSizeClass}' style={{color: colorScheme === 'dark' ? '#66937B' : '#66937B'}} >
                {title}
            </Text>
            </View>
        </View>
        {isHome && title == 'Arosaje' &&(
            <Image
                source={require('../../assets/auth/plante.png')}
                style={{position: 'absolute', top: StatusBar.currentHeight || 0, right: 0, width: 125, height: 300}}
                contentFit="contain"
            />
        )}
    </View>
)

export default Header