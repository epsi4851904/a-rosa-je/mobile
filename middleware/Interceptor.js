import axios from 'axios'
import { useContext, useEffect } from 'react'
import { AuthContext } from '../context/AuthContext'

function useResponseInterceptor() {
    const { isLoggedIn } = useContext(AuthContext);

    useEffect(() => {
        const interceptor = axios.interceptors.response.use(
            function (response) {
                if (response.status === 401) {
                    isLoggedIn()
                }
                return response;
            },
            function (error) {
                return Promise.reject(error);
            }
        )
        return () => {
            axios.interceptors.response.eject(interceptor)
        }
    }, [isLoggedIn])
}

export default useResponseInterceptor
