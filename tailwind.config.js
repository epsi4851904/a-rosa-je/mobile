/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./App.{js,jsx,ts,tsx}",
    './screens/**/*.{js,jsx,ts,tsx}',
    './components/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {},
    colors: {
      'vert': {
        light: '#00cc00',
        dark: '#017C01',
        v2: '#66937B'
      },
      'white': {
        light: '#ffffff'
      }
    }
  },
  plugins: [],
}

