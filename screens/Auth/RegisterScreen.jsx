import React, {useState, useContext} from 'react'
import {
  SafeAreaView,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  useColorScheme,
  Platform,
  StatusBar
} from 'react-native'
import { Alert } from 'react-native'
import InputField from '../../components/InputField'
import api from '../../api.js'
import { AuthContext } from '../../context/AuthContext';
import Ionicons from '@expo/vector-icons/Ionicons';
import { Image } from 'expo-image'; 
import Header from '../../components/Auth/Header.jsx'

const RegisterScreen = ({navigation}) => {
  const [firstName, setFirstName] = useState(null)
  const [lastName, setLastName] = useState()
  const [email, setEmail] = useState(null)
  const [pwd, setPwd] = useState(null)
  const {setIsLoading, setIsValid, setUserToken, setUsername, setPassword} = useContext(AuthContext)
  const [step, setStep] = useState(1)
  const [stepMax, setMaxStep] = useState(1)
  const [confirmPassword, setConfirmPassword] = useState(null)
  const colorScheme = useColorScheme()
  const [show, setShow] = useState(false)
  const onChange = (event, date) => {
    setShow(false)
    setBirthday(date)
  }

  const register = async () => {
    if (!firstName || !lastName || !email || !pwd || !confirmPassword) {
        Alert.alert(
            'Une erreure est survenue',
            'Tous les champs nont pas été remplis',
            [
                { text: 'OK'},
            ],
            { cancelable: false }
        );
        return;
    }
    if (pwd === confirmPassword) {
      setIsLoading(true)
      api.register(firstName, lastName, email, pwd)
      .then((response) => {
          setIsValid(false)
          setUserToken(response.token)
          setUsername(email)
          setPassword(pwd)
      }).finally(() => {
          setIsLoading(false)
      })
    }
  }

  const goToNextStep = () => {
    if (step == 1 && firstName == null && lastName == null) {
      Alert.alert(
        'Eh pas si vite',
        'Il nous faut ton nom et prénom',
        [
          { text: 'j\'y retourne'},
        ],
        { cancelable: false }
      )
      return
    } else if (step == 2 && email == null) {
      Alert.alert(
        'Eh pas si vite',
        'il nous faut ton mail',
        [
            { text: 'j\'y retourne'},
        ],
        { cancelable: false }
      )
      return
    } else if (step == 3 && pwd == null && confirmPassword == null) {
      Alert.alert(
        'Eh pas si vite',
        'il nous faut un mot de passe pour sécuriser ton compte',
        [
            { text: 'j\'y retourne'},
        ],
        { cancelable: false }
      )
      return
    }
    setStep(step => step + 1)
    if(step + 1 != stepMax){
      setMaxStep(step + 1)
    }
    setMaxStep(step + 1)
  }
  
  const StepIcon = ( nextStep ) => {
    if (step >= nextStep || nextStep <= stepMax){
      setStep(nextStep)
    }
  }

  const renderTitle = () => {
    if (step == 1) {
      return "Pas encore de compte ?\n"
    } else if (step == 2) {
      return "Un petit mail ?\n"
    } else {
      return "Un mot de passe ?\n"
    }
  }

  const renderSubTitle = () => {
    if (step == 1) {
      return "Rejoignez-nous !"
    } else if (step == 2) {
      return "Pour vous contacter !"
    } else {
      return "Pour sécuriser votre compte"
    }
  }

  const StepIndicator = ({ currentStep }) => {
    return (
      <View className="flex-row justify-center items-center my-5">
        <TouchableOpacity onPress={() => StepIcon(1)}>
          <Ionicons name="person-outline" size={32} color={currentStep >= 1 ? '#66937B' : '#e0e0e0'} />
        </TouchableOpacity>
        <View className="w-12 h-0.5" style={{ backgroundColor: currentStep >= 2 ? '#66937B' : '#e0e0e0' }} mx-2 />
        <TouchableOpacity onPress={() => StepIcon(2)}>
          <Ionicons name="at-outline" size={32} color={currentStep >= 2 ? '#66937B' : '#e0e0e0'} />
        </TouchableOpacity>
        <View className="w-12 h-0.5" style={{ backgroundColor: currentStep >= 3 ? '#66937B' : '#e0e0e0' }} mx-2 />
        <TouchableOpacity onPress={() => StepIcon(3)}>
          <Ionicons name="key-outline" size={32} color={currentStep >= 3 ? '#66937B' : '#e0e0e0'} />
        </TouchableOpacity>
    </View>

    )
  }

  return (
    <SafeAreaView className="flex-1 justify-center w-full h-screen" style={{backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}}>
      <StatusBar translucent={false} />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={0}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
        >
          <Header isHome={false} navigation={navigation} colorScheme={colorScheme} />
          <View className="items-center">
            <Text className="mt-12 text-3xl font-semibold text-center">
              <Text className="text-white" style={{color: colorScheme === 'dark' ? 'white' : 'black'}}>{renderTitle()}</Text>
              <Text className="text-vert-v2">{renderSubTitle()}</Text>
            </Text>
          </View>
          <View className="flex-1 items-center justify-start mt-10">
            <View className="w-5/6">
              {step === 1 && (
                <View className="items-center">
                  <StepIndicator currentStep={step} />
                  <InputField
                    label={'Prénom'}
                    value={firstName}
                    onChangeText={text => setFirstName(text)}
                  />
                  <InputField
                    label={'Nom'}
                    value={lastName}
                    onChangeText={text => setLastName(text)}
                  />
                  <View className="flex-1 w-full items-end mt-10">
                    <TouchableOpacity className="w-1/3 bg-vert-v2 rounded-full p-3 mt-4 h-10"  onPress={goToNextStep}>
                      <Text className="text-white-light text-center uppercase">suivant</Text>
                    </TouchableOpacity>
                    <Image
                      source={require('../../assets/auth/main_login.svg')}
                      className="absolute top-0 -left-20"
                      style={{ width: '100%', height: 120 }}
                      contentFit="contain"
                    />
                  </View>
                </View>
              )}
              {step === 2 && (
                <View className="items-center">
                  <StepIndicator currentStep={step} />
                  <InputField
                    label={'Email'}
                    value={email}
                    onChangeText={text => setEmail(text)}
                  />
                  <View className="flex-1 w-full items-end mt-10">
                    <TouchableOpacity className="w-1/3 bg-vert-v2 rounded-full p-3 mt-4 h-10"  onPress={goToNextStep}>
                      <Text className="text-white-light text-center">SUIVANT</Text>
                    </TouchableOpacity>
                    <Image
                      source={require('../../assets/auth/main_login.svg')}
                      className="absolute top-0 -left-20"
                      style={{ width: '100%', height: 120 }}
                      contentFit="contain"
                    />
                  </View>
                </View>
              )}
              {step === 3 && (
                <View className="items-center">
                  <StepIndicator currentStep={step} />
                  <InputField
                    inputType={'password'}
                    label={'Mot de passe'}
                    value={pwd}
                    secureTextEntry
                    onChangeText={text => setPwd(text)}
                  />
                  <InputField
                    inputType={'password'}
                    label={'Confirmer le mot de passe'}
                    value={confirmPassword}
                    secureTextEntry
                    onChangeText={text => setConfirmPassword(text)}
                  />
                  <View className="flex-1 w-full items-end mt-10">
                      <TouchableOpacity className="w-1/3 bg-vert-v2 rounded-full p-3 mt-4 h-10"  onPress={register}>
                        <Text className="text-white-light text-center">VALIDER</Text>
                      </TouchableOpacity>
                      <Image
                        source={require('../../assets/auth/main_login.svg')}
                        className="absolute top-0 -left-20"
                        style={{ width: '100%', height: 120 }}
                        contentFit="contain"
                      />
                    </View>
                </View>
              )}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

export default RegisterScreen