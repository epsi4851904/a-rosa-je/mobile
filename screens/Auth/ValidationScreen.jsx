import React, { useContext, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, ScrollView, useColorScheme, SafeAreaView } from 'react-native';
import api from '../../api';
import { AuthContext } from '../../context/AuthContext';
import Header from '../../components/Auth/Header';
import { Image } from 'expo-image'
import InputField from '../../components/InputField';

const VerificationCodeScreen = ({ route, navigation }) => {
    const [verificationCode, setVerificationCode] = useState(['', '', '', ''])
    const {setIsValid, setIsLoading, userToken} = useContext(AuthContext)
    const inputRefs = [useRef(null), useRef(null), useRef(null), useRef(null)]
    const colorScheme = useColorScheme()

    const handleChangeCode = (text, index) => {
      if (text.length <= 1) {
        const newCode = [...verificationCode];
        newCode[index] = text;
        setVerificationCode(newCode);
        if (text.length === 1 && index < 3) {
          inputRefs[index + 1].current.focus();
        }
      }
    };
  
    const handleBackspace = (index) => {
      if (index > 0) {
        const newCode = [...verificationCode];
        newCode[index - 1] = '';
        setVerificationCode(newCode);
        inputRefs[index - 1].current.focus();
      } else if (index === 0) {
        const newCode = [...verificationCode];
        newCode[index] = '';
        setVerificationCode(newCode);
      }
    };

    const handleVerifyCode = async () => {
        setIsLoading(true)
        api.verifCode(verificationCode.join(''), userToken).then((response) => {
            console.log(response.data)
            if (response.success) {
                setIsValid(true)
            }
        }).finally(() => {
            setIsLoading(false)
        })
    };

    return (
      <SafeAreaView className="flex-1" style={{backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}}>
        <ScrollView>
          <Header isHome={true}/>
          <View className="items-center">
            <Text className="mt-12 text-3xl px-2 font-semibold text-center">
              <Text className="text-white" style={{color: colorScheme === 'dark' ? 'white' : 'black'}} >Une dernière étape {'\n'}</Text>
              <Text className="text-vert-v2">Pour valider votre compte, rentrez le code reçu par mail</Text>
            </Text>
          </View>
          <View className="flex-1 items-center justify-start mt-10">
            <View style={styles.container}>
              {verificationCode.map((value, index) => (
                <TextInput
                  key={index}
                  ref={inputRefs[index]}
                  style={[styles.input, value !== '' && styles.active, {color: colorScheme === 'dark' ? 'white' : 'black'}]}
                  placeholder="_"
                  placeholderTextColor="#ccc"
                  onChangeText={(text) => handleChangeCode(text, index)}
                  value={value}
                  keyboardType="numeric"
                  maxLength={1}
                  onKeyPress={({ nativeEvent }) => {
                    if (nativeEvent.key === 'Backspace') {
                      handleBackspace(index);
                    }
                  }}
                />
              ))}
            </View>
            <TouchableOpacity className="bg-vert-v2 rounded-full p-3 w-full mt-4 w-2/3 items-center justify-center" onPress={() => handleVerifyCode()}>
              <Text className="text-white-light text-center">VALIDER</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  input: {
    width: 50,
    height: 50,
    borderWidth: 2,
    borderRadius: 25,
    borderColor: '#ccc',
    fontSize: 24,
    textAlign: 'center',
    marginHorizontal: 5,
  },
  active: {
    borderColor: '#66937B',
  },
});

export default VerificationCodeScreen;
