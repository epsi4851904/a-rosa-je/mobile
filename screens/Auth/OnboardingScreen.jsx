import React, {useContext, useEffect, useState} from 'react'
import { View, Text, TouchableOpacity, Dimensions, Appearance, useColorScheme, SafeAreaView } from 'react-native'
import { AuthContext } from '../../context/AuthContext'
import { Image } from 'expo-image'
import Header from '../../components/Auth/Header'

const OnboardingScreen = ({navigation}) => {
  const {isValid, login, username, password} = useContext(AuthContext);
  const colorScheme = useColorScheme();

  const window = Dimensions.get('window');
  const isSmallScreen = window.width < 360;
  const textSizeClass = isSmallScreen ? "text-lg" : "text-2xl";

  useEffect(() => {
    if (isValid === false) {
      navigation.navigate('Validation')
    } else if (isValid === true) {
      login(username, password)
    }
  }, [])

  return (
    <SafeAreaView className="flex-1" style={{backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}}>
      <Header isHome={true} colorScheme={colorScheme} />
      <View className="w-full justify-start px-5 pt-10"> 
        <Text className="text-left text-white mt-12 text-3xl font-semibold" style={{color: colorScheme === 'dark' ? 'white' : 'black'}}>
          Partagez <Text className="text-vert-v2 ">vos {"\n"}plantes,</Text>
          {"\n"}Trouvez <Text className="text-vert-v2">des {"\n"}conseils !</Text>
        </Text>
      </View>
    
      <View className="items-center justify-center text-center px-4 flex-1"> 
        <TouchableOpacity
          className="bg-vert-v2 p-4 w-3/4 flex-row justify-center rounded-full"
          onPress={() => navigation.navigate('Login')}>
            <Text className="text-white-light text-lg text-center uppercase font-bold">
              C'est parti !
            </Text>
        </TouchableOpacity>
      </View>
      <View className="flex-1 justify-end">
        <Image
          source={require('../../assets/auth/image_view_home.svg')}
          style={{ width: '100%', height: 175 }}
          contentFit="contain"
        />
      </View>
    </SafeAreaView>
  )
}

export default OnboardingScreen;