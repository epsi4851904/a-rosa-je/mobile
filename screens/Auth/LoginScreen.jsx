import { Text, View, TouchableOpacity, SafeAreaView, ScrollView, useColorScheme } from 'react-native'
import React, {useContext, useState} from 'react'
import { AuthContext } from '../../context/AuthContext'
import InputField from '../../components/InputField'
import Header from '../../components/Auth/Header'
import { Image } from 'expo-image'

export default function LoginScreens({ navigation }) {
  const {login} = useContext(AuthContext)
  const colorScheme = useColorScheme()

  const [email, setEmail] = useState(null)
  const [password, setPassword] = useState(null)

  return (
    <SafeAreaView className="flex-1" style={{backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header isHome={false} navigation={navigation} colorScheme={colorScheme} />
        <View className="items-center">
          <Text className="mt-12 text-3xl font-semibold text-center">
            <Text className="text-white" style={{color: colorScheme === 'dark' ? 'white' : 'black'}}>Vous avez un compte ?</Text>
            <Text className="text-vert-v2"> Connectez-vous !</Text>
          </Text>
        </View>
        <View className="flex-1 items-center justify-start mt-10">
          <View className="w-5/6">
            <InputField
              label="Email"
              value={email}
              onChangeText={setEmail}
              keyboardType="email-address"
            />
            <InputField
              inputType="password"
              label="Mot de passe"
              value={password}
              onChangeText={setPassword}
              secureTextEntry={true}
            />
            <TouchableOpacity className="bg-vert-v2 rounded-full p-3 w-full mt-4 items-center justify-center" onPress={() => login(email, password)}>
              <Text className="text-white-light text-center">SE CONNECTER</Text>
            </TouchableOpacity>
            <TouchableOpacity className="mt-4 items-center justify-center" onPress={() =>  navigation.navigate('Register')}>
              <Text className="text-vert-v2 text-center">Pas de compte ? Inscrivez-vous</Text>
            </TouchableOpacity>
            <View className="flex-1 justify-end">
              <Image
                source={require('../../assets/auth/main_login.svg')}
                className="absolute top-10 -left-20"
                style={{ width: '100%', height: 150 }}
                contentFit="contain"
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}