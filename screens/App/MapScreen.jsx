import React, { useState, useEffect, useContext, useCallback, useMemo, useRef } from 'react'
import { StyleSheet, StatusBar, View, Image, Text, TouchableOpacity, Animated, Dimensions, Platform } from 'react-native'
import { AuthContext } from '../../context/AuthContext'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import * as Location from 'expo-location'
import { APP_URL } from '../../config'
import axios from 'axios'
import { useNavigation } from '@react-navigation/native'
import Ionicons from '@expo/vector-icons/Ionicons'
import mapStyle from '../../components/Map/mapStyle'

const {width, height} = Dimensions.get('window')
const CARD_WIDTH = width * 0.8

export default function MapScreen() {
    const navigation = useNavigation()
    const { userToken } = useContext(AuthContext);
    const [location, setLocation] = useState(null);
    const [posts, setPosts] = useState([])
    const _map = useRef(null)
    const [viewAll, setViewAll] = useState(false)
    const [index, setIndex] = useState(0)

    const handleMarkerPress = (postIndex) => {
        setIndex(CARD_WIDTH * postIndex)
        if (!viewAll){
            setViewAll(true)
        }
        scrollViewRef?.current?.scrollTo({
            x: CARD_WIDTH * postIndex,
            animated: true,
        });
    };

    let mapIndex = -1
    let mapAnimation = new Animated.Value(index)

    const getPosts = async () => {
        const options = {
            method: 'GET',
            url: `${APP_URL}/api/post/map`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        axios.request(options).then((response) => {
            setPosts(response.data)
        }).catch((error) => {
            console.error(error);
        })
    };

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            let location = await Location.getCurrentPositionAsync({})
            setLocation({
                center: {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                },
                pitch: 0,
                heading: 0,
                altitude: 100,
                zoom: 12,
            })
        })()
        getPosts()
    }, [])

    mapAnimation.addListener(({value}) => {
        let index = Math.floor(value / CARD_WIDTH + 0.3)
        console.log("index" + index)
        if (index >= posts.length) {
            index = posts.length - 1
        }
        if (index <= 0) {
            index = 0
        }

        clearTimeout(regionTimeout)

        const regionTimeout = setTimeout(() => {
            if (viewAll && mapIndex !== index) {
                mapIndex = index
                const post = posts[index]
                _map.current.animateToRegion(
                    {
                        ...post,
                        latitudeDelta: 0.1,
                        longitudeDelta: 0.1
                    },
                    350
                )
            }
        }, 10)
    })

    const renderImageMain = (images) => {
        const image = images.filter(item => item.isMain === true)
        return image[0]
    }

    const scrollViewRef = useRef(null);

    return (
        <View  style={styles.container}>
            <StatusBar translucent={false} />
            <MapView
                ref={_map}
                style={styles.map}
                initialCamera={location}
                showsUserLocation={true}
                followUserLocation={true}
                rotationEnabled={false}
                customMapStyle={mapStyle}
            >
                {posts.map((post, index) => (
                    <Marker
                        key={post.id}
                        coordinate={{ latitude: post.latitude, longitude: post.longitude }}
                        onPress={() => handleMarkerPress(index)}
                    >
                        <View style={styles.markerContainer}>
                            <Image
                                source={{ uri: APP_URL + renderImageMain(post.images).name }}
                                style={styles.markerImage}
                            />
                        </View>
                    </Marker>
                ))}
            </MapView>
            {posts.length > 0 && (
                <View style={[styles.scrollView, {flex: 1, justifyContent: 'center', alignItems: viewAll ? 'flex-end' : 'center', marginRight: viewAll ? '5%' : 0, bottom: viewAll ? '31%' : '10%'}]}>
                    <TouchableOpacity onPress={() => setViewAll(!viewAll)} style={{backgroundColor: 'rgba(255,255,255,0.9)', padding: 10, borderWidth: 1, borderRadius: 20, flexDirection: 'row'}}>
                        <Text>{viewAll ? 'Fermer' : 'Voir toutes les plantes'}</Text>
                        <Ionicons name={viewAll ? 'chevron-down-outline' : 'chevron-up-outline'} color="black" size={15} />
                    </TouchableOpacity>
                </View>
            )}
            {viewAll && (
                <Animated.ScrollView
                    ref={scrollViewRef}
                    horizontal
                    scrollEventThrottle={16}
                    showsHorizontalScrollIndicator={false}
                    style={styles.scrollView}
                    pagingEnabled
                    snapToInterval={CARD_WIDTH + 20}
                    snapToAlignment="center"
                    decelerationRate={0.2}
                    onScroll={Animated.event(
                        [
                            {
                                nativeEvent: {
                                    contentOffset: {
                                        x: mapAnimation
                                    }
                                }
                            }
                        ],
                        {useNativeDriver: true}
                    )}
                >
                    {posts.map((post, index) => (
                        <TouchableOpacity key={index} onPress={() => navigation.navigate('post_detail', {id: post.id})}>
                            <View style={styles.card}>
                                <Image
                                    source={{ uri: APP_URL + renderImageMain(post.images).name }}
                                    className="w-full h-full"
                                    resizeMode="cover"
                                />
                               <View className="absolute right-1 bottom-1 bg-[#66937B70] rounded-lg px-2 py-1">
                                    <Text className="text-white-light text-xl font-bold">{post.namePlant}</Text>
                                </View>

                            </View>
                        </TouchableOpacity>
                    ))}
                </Animated.ScrollView>
            )}
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    map: {
        width: '100%',
        height: '100%',
    },
    bottomSheetContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20, 
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10, 
    },
    button: {
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 20,
    },
    buttonText: {
        fontSize: 16,
        color: '#ffffff',
    },
    closeButton: {
        backgroundColor: '#66937B', 
    },
    detailButton: {
        backgroundColor: '#66937B', 
    },
    postContent: {
        width: '100%', 
        alignItems: 'center',
    },  
    postTitle: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    loremStyle: {
        fontSize: 16,
        color: '#999',
    },
    postDescription: {
      fontSize: 16,
      textAlign: 'center',
      color: '#666',
      marginTop:10,
      paddingHorizontal: 20,
    },
    postImage: {
        marginTop:10,
        width: 150,
        height: 150,
        marginBottom: 20,
    },
    bottomSheetContainer: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    postContent: {
        width: '100%',
        alignItems: 'center',
        marginTop: 20, 
    },
    markerContainer: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#66937B', 
        borderRadius: 25, 
        shadowColor: '#66937B', 
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5, 
    },
    markerImage: {
        width: 40, 
        height: 40,
        borderRadius: 20, 
    },
    userDetailContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
    },
    userAvatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10,
        backgroundColor: "#66937B"
    },
    userName: {
        fontWeight: 'bold',
    },
    postLocation: {
        color: '#aaa',
        marginLeft: 5, 
    },
    postLocationContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    scrollView: {
        position: 'absolute',
        bottom: '10%',
        left: 0,
        right: 0,
        paddingVertical: 10
    },
    card: {
        elevation: 2,
        backgroundColor: '#fff',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        marginHorizontal: 10,
        shadowColor: '#000',
        shadowRadius: 5, 
        shadowOpacity: 0.3,
        shadowOffset: {x: 2, y: -2},
        height: 150,
        width: CARD_WIDTH,
        overflow: 'hidden'
    }
});