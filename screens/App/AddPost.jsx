import React, { useState, useEffect, useContext } from 'react'
import { AuthContext } from '../../context/AuthContext'
import { KeyboardAvoidingView, StyleSheet, Text, View, ScrollView, Image, TouchableOpacity , Alert, Platform, useColorScheme, TextInput, Keyboard} from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import Ionicons from '@expo/vector-icons/Ionicons'
import { APP_URL } from '../../config.js'
import DateTimePicker from '@react-native-community/datetimepicker'
import Header from '../../components/Auth/Header.jsx'
import InputField from '../../components/InputField.jsx'
import CheckBox from 'expo-checkbox'
import * as Location from 'expo-location'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useFocusEffect } from '@react-navigation/native'

export default function AddPost() {
    const { setIsLoading, userToken} = useContext(AuthContext)
    const [giveLocation, setGiveLocation] = useState(true)
    const [imageSource, setImageSource] = useState(null)
    const [plantName, setPlantName] = useState('')
    const [description, setDescription] = useState('')
    const [city, setCity] = useState('')
    const [zipcode, setZipcode] = useState('')
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
    const [showStartDatePicker, setShowStartDatePicker] = useState(false)
    const [showEndDatePicker, setShowEndDatePicker] = useState(false)
    const colorScheme = useColorScheme()
    const [scrollPosition, setScrollPosition] = useState(0)
    const [isExpanded, setIsExpanded] = useState(false)

    const isModif = () => {
        return plantName != '' && city != '' && zipcode != '' && description != '' && imageSource != ''
    }

    useFocusEffect(
        React.useCallback(() => {
            (async () => {
                const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync()
                if (status !== 'granted') {
                    alert("Une autorisation d'accès à la bibliothèque multimédia est requise !")
                }
            })()
            showOptions()
        }, []))

    const selectImage = async (sourceType) => {
        let result;
        if (Platform.OS == "ios") {
            const { status } = await ImagePicker.requestCameraPermissionsAsync()
            if (status !== "granted") {
                alert("Permission denied to access camera roll")
                return
            }
        }
        if (sourceType === 'gallery') {
            result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                quality: 0.7,
            })
        } else if (sourceType === 'camera') {
            result = await ImagePicker.launchCameraAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                quality: 0.7,
            })
        }
    
        if (result) {
            if (!result.cancelled && result.assets.length > 0 && result.assets[0].uri) {
                const imageType = result.assets[0].type || 'image/jpeg'
                await setImageSource({ uri: result.assets[0].uri, type: imageType })
            } else {
                console.error("Image selection was cancelled or URI is undefined.")
            }
        } else {
            console.error("Error occurred during image selection.")
        }
    };

    const handleStartDateChange = (event, selectedDate) => {
        setShowStartDatePicker(false)
        if (event.type === 'dismissed') {
            setStartDate(null)
        } else if (selectedDate) {
            if(endDate){
                if (selectedDate > endDate) {
                    alert("La date de debut ne peut pas être postérieure à la date de fin.")
                } else {
                    setStartDate(selectedDate)
                }
            }else {
                setStartDate(selectedDate)
            }
        }
    };

    const handleEndDateChange = (event, selectedDate) => {
        setShowEndDatePicker(false);
    if (event.type === 'dismissed') {
        setEndDate(null)
    } else if (selectedDate) {
        if (selectedDate < startDate) {
            alert("La date de fin ne peut pas être antérieure à la date de début.")
        } else {
            setEndDate(selectedDate)
        }
    }
    }

    const showStartDatePickerModal = () => {
        setShowStartDatePicker(true)
    }

    const showEndDatePickerModal = () => {
        setShowEndDatePicker(true)
    }

    const showOptions = () => {
        Alert.alert(
            'Choisir une image',
            'Souhaitez-vous prendre une nouvelle photo ou sélectionner depuis la galerie ?',
            [
                { text: 'Retour', style: 'cancel' },
                { text: 'Prendre une Photo', onPress: () => selectImage('camera') },
                { text: 'Choisir depuis la Gallerie', onPress: () => selectImage('gallery') }
            ]
        )
    }

    const postPost = async () => {
        
        setIsLoading(true)
        const formData = new FormData()
        formData.append("namePlant", plantName)
        formData.append("description", description)
        formData.append("city", city)
        formData.append("zipcode", zipcode)
        if (startDate != null) {
            formData.append("startAt", startDate.toISOString())
        }
        if (endDate != null) {
            formData.append("endAt", endDate.toISOString())
        }
        if (giveLocation) {
            let location = await Location.getCurrentPositionAsync({})
            formData.append("latitude", location.coords.latitude)
            formData.append("longitude", location.coords.longitude)
        }
        const file = {
            uri: imageSource.uri,
            name: 'photo.jpg', 
            type: 'image/jpeg',
        }
        formData.append('image', file)
    
        try {
            const response = await fetch(`${APP_URL}/api/post`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${userToken}`,
                },
                body: formData,
            })
    
            const responseData = await response.json()
            setImageSource(null)
            setPlantName(null)
            setDescription(null)
            setCity(null)
            setZipcode(null)
            setStartDate(null)
            setEndDate(null)

        } catch (error) {

            console.error('Error:', error)
        } finally {
            setIsLoading(false)
        }
    }

    return (
        <View style={[{flex: 1}, {backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}]}>
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.select({ ios: 0, android: 75 })}
            >
                {imageSource && (
                    <Image
                        source={imageSource}
                        resizeMode="cover"
                        style={{position: 'absolute', width: '100%', height: 300}}
                    />
                )}
                <View className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute top-10 right-2 z-10">
                    <TouchableOpacity onPress={showOptions}>
                        <Ionicons name="create-outline" color="white" size={20} />
                    </TouchableOpacity>
                </View>
                {imageSource && isExpanded && (
                        <View className="bg-[#333333]/70 h-screen flex-1 justify-center items-center" style={{position: 'absolute', width: '100%', top: -50, left: 50,zIndex: 999 ,transform:[{translateY: 50},{translateX: -50}]}}>
                            <TouchableOpacity className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute z-10 top-10 right-2" onPress={() => setIsExpanded(false)}>
                                <Ionicons name="close-outline" color="white" size={25} />
                            </TouchableOpacity>
                            <Image
                                source={imageSource}
                                resizeMode="cover"
                                style={{width: '95%', height: '80%'}}
                                className="rounded-lg"
                            />
                        </View>
                )}
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow: 1, paddingTop: 250}}>
                    <View className="h-8 w-8 bg-[#333333]/80 flex items-center justify-center rounded-full ml-auto mr-5">
                        <TouchableOpacity onPress={() => setIsExpanded(true)}>
                            <Ionicons name="expand-outline" color="white" size={20} />
                        </TouchableOpacity>
                    </View>
                    <View className="flex-1 pt-5 px-2 rounded-t-lg" style={{backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}}>
                        <View className="flex-row justify-center align-center mt-2">
                            <View style={{ flex: 1 }}>
                                <TextInput style={[{ fontWeight: 'bold' }]} className="text-2xl text-vert-v2 bg-[#D3D3D3]/20 rounded-lg p-1"
                                    label="Nom de la plante"
                                    value={plantName}
                                    onChangeText={setPlantName}
                                    placeholder='Nom de votre plante'
                                />
                                <View style={{ color: '#aaa'}} className="py-1 flex-row align-center">
                                    <Ionicons name="location-sharp" color="#aaa" size={15}/>
                                    <TextInput style={[{ fontWeight: 'bold', minWidth: 100 }]} className=" mr-2 bg-[#D3D3D3]/20 rounded-lg p-1 text-[#fff]"
                                        label="Ville"
                                        value={city}
                                        onChangeText={setCity}
                                        placeholder='Ville'
                                    />
                                    <Text style={{ color: '#aaa'}}>-</Text>
                                    <TextInput style={[{ fontWeight: 'bold', minWidth: 100 }]} className="ml-2 bg-[#D3D3D3]/20 rounded-lg p-1 text-[#fff]"
                                        label="code postal"
                                        value={zipcode}
                                        onChangeText={setZipcode}
                                        placeholder='Code postal'
                                    />
                                </View>
                                <View className="py-1 flex-column align-center">
                                    <View className="flex-row">
                                        <Ionicons name="calendar-outline" color="#aaa" size={15}/>
                                        <Text style={{ color: '#aaa'}} className="ml-2">
                                            Un gardiennage ?
                                        </Text>    
                                    </View>
                                    <View className="flex-row align-center items-center">
                                        <Text style={{color: colorScheme == 'dark' ? '#F5F5F5' : 'black'}}>Du</Text>
                                        <TouchableOpacity 
                                            onPress={showStartDatePickerModal}
                                            className="p-2 m-2 rounded-lg"
                                            style={{backgroundColor: colorScheme == 'dark' ? '#333333' : 'white', borderColor: colorScheme == 'dark' ? '#333333' : 'black'}}
                                        >
                                            <Text style={{color: colorScheme === 'dark' ? 'grey' : 'black'}}>{startDate ? startDate.toLocaleDateString() : 'Début'}</Text>
                                        </TouchableOpacity>
                                        <Text style={{color: colorScheme == 'dark' ? '#F5F5F5' : 'black'}}>au</Text>
                                        <TouchableOpacity 
                                            onPress={showEndDatePickerModal}
                                            className="p-2 m-2 rounded-lg"
                                            style={{backgroundColor: colorScheme == 'dark' ? '#333333' : 'white', borderColor: colorScheme == 'dark' ? '#333333' : 'black'}}
                                        >
                                            <Text style={{color: colorScheme === 'dark' ? 'grey' : 'black'}}>{endDate ? endDate.toLocaleDateString() : 'Fin'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {showStartDatePicker && (
                                    <DateTimePicker
                                        value={startDate || new Date()}
                                        mode="date"
                                        display={Platform.OS === 'ios' ? 'spinner' : 'default'}
                                        onChange={handleStartDateChange}
                                    />
                                )}
                                {showEndDatePicker && (
                                    <DateTimePicker
                                        value={endDate || new Date()}
                                        mode="date"
                                        display={Platform.OS === 'ios' ? 'spinner' : 'default'}
                                        onChange={handleEndDateChange}
                                    />
                                )}
                            </View>
                        </View>
                        <View className="flex flex-row justify-between">
                            <Text className="w-5/6 text-[#A9A9A9]">J'accepte de donner la localisation de ma photo ?</Text>
                            <View className="w-1/6 pl-2">
                                <CheckBox
                                    value={giveLocation}
                                    onValueChange={setGiveLocation}
                                    color={giveLocation ? '#66937B' : undefined}
                                />
                            </View>
                        </View>
                        <View className="mt-5 px-2 text-sm">
                            <Text className="flex-row align-center text-xl" style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                                <Ionicons name="leaf-outline" color="#66937B" size={20}/>
                                Description
                            </Text>
                            <TextInput 
                                className="bg-[#D3D3D3]/20 rounded-lg p-1 text-[#fff] h-36" 
                                style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333', textAlignVertical: 'top'}}
                                label="description"
                                value={description}
                                onChangeText={setDescription}
                                multiline
                                placeholder='Votre texte...'
                            />
                        </View>
                        {isModif() && (
                            <View className="flex-row justify-center mb-20">
                                <TouchableOpacity onPress={postPost} className="bg-vert-v2 rounded-full p-3 w-full flex-row flex-column mt-4 justify-center">
                                    <Text className="text-white text-lg uppercase mr-2">Partager</Text>
                                    <Ionicons name={"share-outline"} color="black" size={24} />
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </View>
    )
}

const styles = StyleSheet.create({
    box: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        padding: 15,
        margin: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 2,
    },
    boxTitle: {
        fontSize: 15,
        textAlign: 'center'
    },
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    textInput: {
        flex: 1,
        height: 40,
        borderWidth: 1,
        borderRadius: 20,
        paddingHorizontal: 10,
    },
    sendButton: {
        marginLeft: 10,
        backgroundColor: '#017C01',
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    sendButtonText: {
        color: '#ffffff',
        fontWeight: 'bold',
    },
})