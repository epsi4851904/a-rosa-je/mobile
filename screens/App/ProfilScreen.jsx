import React, { useContext, useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ScrollView, TouchableOpacity, Image, useColorScheme, Platform } from 'react-native'
import { AuthContext } from '../../context/AuthContext'
import Ionicons from '@expo/vector-icons/Ionicons'
import { useFocusEffect, useNavigation } from '@react-navigation/core'
import Box from '../../components/Box'
import { APP_URL } from '../../config'
import axios from 'axios'


export default function ProfilScreen() {
  const {userInfo, userToken} = useContext(AuthContext)
  const user = userInfo
  const navigation = useNavigation()
  const [posts, setPosts] = useState([])
  const [keeped, setKeeped] = useState([])
  const [waiting, setWaiting] = useState([])
  const [selectedFilter, setSelectedFilter] = useState('Tout')
  const colorScheme = useColorScheme()

  const getUserPost = async () => {
    const options = {
      method: 'GET',
      url: `${APP_URL}/api/profil/posts/${user.id}`,
      headers: {
        Authorization: `Bearer ${userToken}`
      }
    }
    await axios.request(options).then((response) => {
      setPosts(response.data.posts)
      setKeeped(response.data.keeping)
      setWaiting(response.data.waiting)
    }).catch((error) => {
      console.error(error)
    })
  }

  const handleRefresh = async () => {
    await getUserPost()
  }

  useFocusEffect(
      React.useCallback(() => {
          handleRefresh()
      }, [])
  )

  const handleFilterSelection = async (filter) => {
    setSelectedFilter(filter);

    let filteredPosts = [];

    if (filter === 'Tout') {
      filteredPosts = posts;
    } else if (filter === 'Gardiennage') {
      filteredPosts = posts.filter((item) => item.endAt !== null && item.startAt !== null);
    } else if (filter === 'Conseil') {
      filteredPosts = posts.filter((item) => item.endAt === null && item.startAt === null);
    }
  }


  return (
    <SafeAreaView style={[{ flex: 1 }, {paddingTop: Platform.OS == 'android' ? 25 : 0, paddingBottom: 50, backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}]}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{paddingVertical: 20, flex: 1}}>
          <View style={{paddingHorizontal: 15, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={[styles.title, {color: colorScheme === 'dark' ? 'white' : 'black'}]}>
              {user.firstName} {user.lastName}
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Add Post')}>
              <Ionicons name="add-circle-outline" color={colorScheme === 'dark' ? 'white' : 'black'} size={25} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('settings_modal')}>
              <Ionicons name="menu" color={colorScheme === 'dark' ? 'white' : 'black'} size={25} />
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}} className="my-5">
            <Image 
                source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ user.firstName + user.lastName +'&backgroundColor[]' }} 
                style={{ width: 75, height: 75, borderRadius: 100, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
                className="border-1"
            />
            <View>
              <Text className="text-center" style={[{color: colorScheme === 'dark' ? 'white' : 'black'}]}>
                {posts.length}
              </Text>
              <Text style={[{color: colorScheme === 'dark' ? 'white' : 'black'}]}>post</Text>
            </View>
            <View>
              <Text className="text-center" style={[{color: colorScheme === 'dark' ? 'white' : 'black'}]}>
                {keeped.length}
              </Text>
              <Text style={[{color: colorScheme === 'dark' ? 'white' : 'black'}]}>gardiennage</Text>
            </View>
            <View>
              <Text className="text-center" style={[{color: colorScheme === 'dark' ? 'white' : 'black'}]}>
                0
              </Text>
              <Text style={[{color: colorScheme === 'dark' ? 'white' : 'black'}]}>note</Text>
            </View>
          </View>
          <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}>
            <View className="w-full flex-row justify-start items-center m-5">
              <TouchableOpacity
                className="px-4 py-2 rounded-xl"
                style={{backgroundColor: selectedFilter === 'Tout' ? '#66937B' : 'transparent'}}
                onPress={() => handleFilterSelection('Tout')}
              >
                <Text style={{color: colorScheme === 'dark' || selectedFilter === 'Tout' ? 'white' : 'black'}}>Mes plantes</Text>
              </TouchableOpacity>
              <TouchableOpacity
                className="px-4 py-2 rounded-xl"
                style={{backgroundColor: selectedFilter === 'Gardiennage' ? '#66937B' : 'transparent'}}
                onPress={() => handleFilterSelection('Gardiennage')}
              >
                <Text style={{color: colorScheme === 'dark' || selectedFilter === 'Gardiennage' ? 'white' : 'black'}}>Mes gardiennages</Text>
              </TouchableOpacity>
              <TouchableOpacity
                className="px-4 py-2 rounded-xl"
                style={{backgroundColor: selectedFilter === 'Conseil' ? '#66937B' : 'transparent'}}
                onPress={() => handleFilterSelection('Conseil')}
              >
                <Text style={{color: colorScheme === 'dark' || selectedFilter === 'Conseil' ? 'white' : 'black'}}>En attente</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
            {selectedFilter == 'Tout' && (
              posts.map((item) => (
                <Box
                  key={item.id}
                  post={item}
                  detail={() => navigation.navigate('post_detail', {id: item.id})}
                  isProfil={true}
                  url="/images/post/"
                  update={() => navigation.navigate('update_post', {id: item.id})}
                  userToken={userToken}
                />
                
              ))
            )}
            {selectedFilter == 'Gardiennage' && (
              keeped.map((item) => (
                <Box
                  key={item.id}
                  post={item}
                  detail={() => navigation.navigate('post_detail', {id: item.id})}
                  url="/images/post/"
                />
                
              ))
            )}
            {selectedFilter == 'Conseil' && (
              waiting.map((item) => (
                <Box
                  key={item.id}
                  post={item}
                  detail={() => navigation.navigate('post_detail', {id: item.id})}
                  url="/images/post/"
                />
                
              ))
            )}
          </View>
        </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    paddingTop: 20
  },
  box: {
    backgroundColor: '#ffffff',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: '70%',
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2,
  },
  boxTitle: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  title: {
    fontSize: 25
  }
});