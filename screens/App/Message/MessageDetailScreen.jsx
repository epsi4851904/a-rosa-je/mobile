import React, { useContext, useEffect, useLayoutEffect, useState } from "react"
import { AuthContext } from "../../../context/AuthContext";
import { useFocusEffect, useNavigation, useRoute } from "@react-navigation/native"
import { SafeAreaView, Text, FlatList, Image, StyleSheet, View, TextInput, TouchableOpacity, KeyboardAvoidingView, Platform, useColorScheme } from "react-native"
import Ionicons from '@expo/vector-icons/Ionicons'
import { APP_URL, APP_WS } from "../../../config"
import axios from "axios"
import io from 'socket.io-client'

export default function MessageDetail({ navigation }) {
    const {userToken, userInfo} = useContext(AuthContext)
    const [messages, setMessages] = useState([])
    const [newMessage, setNewMessage] = useState('')
    const route = useRoute()
    const id = route.params.id
    const firstName = route.params.firstName
    const lastName = route.params.lastName
    const user = userInfo
    const [page, setPage] = useState('home')
    const colorScheme = useColorScheme()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerTitleAlign: 'left',
            headerTitle: () => (
                <TouchableOpacity className="mr-5 flex-row justify-center items-start" onPress={() => navigation.navigate('user_detail', {id: id})}>
                    <Image 
                        source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ firstName+lastName +'&backgroundColor[]' }} 
                        style={{ width: 40, height: 40, borderRadius: 25, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
                        className="border-1"
                    />
                    <View className="flex-column">
                        <Text style={{fontSize: 18, color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                            {firstName} {lastName}
                        </Text>
                        <View className="flex-row items-center mt-1">
                            <Ionicons name="ellipse" color="#66937B" size={10} />
                            <Text className="ml-1" style={{fontSize: 12, color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                                En ligne
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        });
    }, [navigation, page]);

    const getMessageById = () => {
        const options = {
            method: 'GET',
            url: `${APP_URL}/api/message/${id}`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        axios.request(options).then((response) => {
            setMessages(response.data)
            const unseenMessageIds = response.data
                .filter(message => message.sender.id !== user.id && message.viewAt === null)
                .map(message => message.id)
            if (unseenMessageIds.length > 0) {
                viewMessage(unseenMessageIds.join(','))
            }
        }).catch((error) => {
            console.error(error)
        })
    }

    const viewMessage = (ids) => {
        const options = {
            method: 'POST',
            url: `${APP_URL}/api/message/view/${ids}`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        axios.request(options).then((response) => {
            return response.data
        }).catch((error) => {
            console.error(error.config)
        })
    }

    const handleRefresh = async () => {
        await getMessageById()
    }

    useFocusEffect(
        React.useCallback(() => {
            const socket = io('http://172.20.10.2:4000')

            try {
                socket.on(`message/${user.id}/${id}`, async (data) => {
                    await setMessages(message =>  [...message, data] )
                    reversedMessages = messages.slice().reverse()
                    await viewMessage(data.id)
                })
            } catch (err) {
                console.error(err)
            }

            handleRefresh()
        }, [])
    )
    let reversedMessages = messages.slice().reverse()
    const sendMessage = () => {
        if (newMessage.trim() !== '') {
            const form = new FormData();
            form.append('message', newMessage)
            const options = {
                method: 'POST',
                url: `${APP_URL}/api/message/send/${id}`,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${userToken}`
                },
                data: form
            }
            axios.request(options).then((response) => {
                const newMessageItem = response.data
                setMessages([...messages, newMessageItem])
                reversedMessages = messages.slice().reverse()
                setNewMessage('')
            }).catch((error) => {
                console.error(error.config)
            })
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5' }}>
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={100}>
                <View style={{ flex: 1 }} className="px-2">
                    <FlatList
                        data={reversedMessages}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (
                            <View style={[styles.contain ,item.sender.id == user.id ? styles.ownMessage : null]}>
                                {item.sender.id != user.id && (
                                    <TouchableOpacity onPress={() => navigation.navigate('user_detail', {id: id})}>
                                        <Image 
                                            source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ item.sender.firstName + item.sender.lastName +'&backgroundColor[]' }} 
                                            style={{ width: 25, height: 25, borderRadius: 25, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
                                            className="border-1"
                                        />
                                    </TouchableOpacity>
                                )}
                                <View style={[styles.messageBubble, item.sender.id == user.id ? styles.ownMessageBubble : null]}>
                                    <Text style={{color: '#F5F5F5'}}>
                                        {item.text}
                                    </Text>
                                </View>
                            </View>
                        )}
                        inverted
                    />
                </View>
                <View style={styles.inputContainer}>
                    <TextInput
                        multiline={true}
                        textAlignVertical="center"
                        style={[styles.textInput, styles.shadowColor, {backgroundColor: colorScheme === 'dark' ? '#333333' : '#ffffff'}]}
                        placeholder="Entrez votre message"
                        value={newMessage}
                        onChangeText={text => setNewMessage(text)}
                    />
                    {newMessage.trim() !== '' && (
                        <TouchableOpacity style={styles.sendButton} onPress={sendMessage}>
                            <Ionicons name="paper-plane-outline" color='white' size={20} />
                        </TouchableOpacity>
                    )}
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    contain: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    messageBubble: {
        maxWidth: '80%',
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
        alignSelf: 'flex-start',
        color: '#ffffff',
        backgroundColor: '#2B4133',
    },
    ownMessageBubble: {
        alignSelf: 'flex-end',
        backgroundColor: '#66937B',
    },
    ownMessage: {
        alignSelf: 'flex-end',
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    textInput: {
        flex: 1,
        height: 40,
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingTop: 5
    },
    sendButton: {
        marginLeft: 10,
        backgroundColor: '#017C01',
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    sendButtonText: {
        color: '#ffffff',
        fontWeight: 'bold',
    },
    shadowColor: '#000', 
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
});