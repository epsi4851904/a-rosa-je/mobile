import { TouchableOpacity, ScrollView, View, Text, useColorScheme, SafeAreaView, StatusBar, Platform } from 'react-native'
import React, { useContext, useState, useEffect } from 'react'
import { AuthContext } from '../../context/AuthContext'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import Box from '../../components/Box'
import { APP_URL } from '../../config'
import axios from 'axios'
import Header from '../../components/Auth/Header'
import InputField from '../../components/InputField'
import Ionicons from '@expo/vector-icons/Ionicons'

export default function DashboardScreen() {
  const {setIsLoading, userToken, isLoggedIn} = useContext(AuthContext)
  const navigation = useNavigation()
  const [user, setUser] = useState([])
  const [posts, setPosts] = useState([])
  const [selectedFilter, setSelectedFilter] = useState('Tout')
  const [postFiltered, setPostFiltered] = useState([])
  const [search, setSearch] = useState('')
  const colorScheme = useColorScheme()

  useEffect(() => {
    async function checkAutorisations() {
      const autorisationsChecked = await AsyncStorage.getItem('autorisationsChecked')
      if (!autorisationsChecked) {
        navigation.navigate("autorisation_screen")
      }
    }
    isLoggedIn()
    checkAutorisations()
  }, [navigation])


  const checkUserConnected = async () => {
    const userInfoString = await AsyncStorage.getItem('userInfo');
    if (userInfoString) {
        setUser(JSON.parse(userInfoString))
    } else {
        const token = await AsyncStorage.getItem('userToken');
        if (!token) {
          throw new Error('Token non trouvé dans AsyncStorage');
        }
    }
  }

  const getPosts = async (text = '') => {
    setSearch(text)
    const options = {
      method: 'GET',
      url: `${APP_URL}/api/post`,
      headers: {
        Authorization: `Bearer ${userToken}`
      },
      params: {
        search: text
      }
    }
    axios.request(options).then((response) => {
      setPosts(response.data)
      setPostFiltered(response.data)
    }).catch((error) => {
      console.error(error)
    })
  }

  const handleRefresh = async () => {
    await checkUserConnected()
    await getPosts(search)
    setSelectedFilter('Tout')
  }

  useFocusEffect(
    React.useCallback(() => {
      handleRefresh()
    }, [])
  )

  const handleFilterSelection = async (filter) => {
    setSelectedFilter(filter);

    let filteredPosts = [];

    if (filter === 'Tout') {
      filteredPosts = posts;
    } else if (filter === 'Gardiennage') {
      filteredPosts = posts.filter((item) => item.endAt !== null && item.startAt !== null);
    } else if (filter === 'Conseil') {
      filteredPosts = posts.filter((item) => item.endAt === null && item.startAt === null);
    }

    setPostFiltered(filteredPosts);
  }


  return (
    <SafeAreaView style={[{ flex: 1 }, {paddingTop:Platform.OS == 'android' ? 25 : 0 ,backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5', paddingBottom: 50}]}>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <StatusBar style="auto" />
        <Header isHome={true} />
        <View className="w-full px-2">
          <InputField
            icon={<Ionicons name="search-outline" color="grey" size={20}/>}
            label="Rechercher"
            value={search}
            onChangeText={(text) => getPosts(text)}
            keyboardType="web-search"
          />
        </View>
        <View className="w-full flex-row justify-start items-center m-5">
          <TouchableOpacity
            className="px-4 py-2 rounded-xl"
            style={{backgroundColor: selectedFilter === 'Tout' ? '#66937B' : 'transparent'}}
            onPress={() => handleFilterSelection('Tout')}
          >
            <Text style={{color: colorScheme === 'dark' || selectedFilter === 'Tout' ? 'white' : 'black'}}>Tout</Text>
          </TouchableOpacity>
          <TouchableOpacity
            className="px-4 py-2 rounded-xl"
            style={{backgroundColor: selectedFilter === 'Gardiennage' ? '#66937B' : 'transparent'}}
            onPress={() => handleFilterSelection('Gardiennage')}
          >
            <Text style={{color: colorScheme === 'dark' || selectedFilter === 'Gardiennage' ? 'white' : 'black'}}>Gardiennage</Text>
          </TouchableOpacity>
          <TouchableOpacity
            className="px-4 py-2 rounded-xl"
            style={{backgroundColor: selectedFilter === 'Conseil' ? '#66937B' : 'transparent'}}
            onPress={() => handleFilterSelection('Conseil')}
          >
            <Text style={{color: colorScheme === 'dark' || selectedFilter === 'Conseil' ? 'white' : 'black'}}>Conseil</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'flex-start' }}>
          {postFiltered.map((item) => (
            <Box
              key={item.id}
              post={item}
              detail={() => navigation.navigate('post_detail', {id: item.id})}
            />
            
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
