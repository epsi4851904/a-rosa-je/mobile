import { useContext, useEffect, useLayoutEffect, useState } from "react";
import { AuthContext } from "../../../context/AuthContext"
import { StyleSheet, Text, View, SafeAreaView, ScrollView, TouchableOpacity, Alert, usetheme, Switch, Platform, Linking, useColorScheme } from 'react-native'
import Ionicons from '@expo/vector-icons/Ionicons'
import InputField from "../../../components/InputField";
import { APP_URL } from "../../../config";
import axios from "axios";
import RadioButton from "../../../components/RadioButton";

export default function SettingsModal({ navigation }) {
    const {logout, userInfo, userToken, setUserInfo} = useContext(AuthContext)
    const user = userInfo
    const [firstName, setFirstName] = useState(user.firstName)
    const [lastName, setLastName] = useState(user.lastName)
    const [email, setEmail] = useState(user.email)
    const [page, setPage] = useState('home')
    const theme = useColorScheme()

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <TouchableOpacity className="ml-2" onPress={() => renderAction()}>
                    <Ionicons name={renderIcon()} color={theme == 'dark' ? 'white' : 'black'} size={30} />
                </TouchableOpacity>
            ),
            headerTitle: renderTitle()
        });
    }, [navigation, page]);

    const renderIcon = () => {
        if (page == 'home') {
            return 'close'
        } else if (page == 'info' || page == 'modif' || page == 'theme') {
            return 'chevron-back-outline'
        }
    }

    const renderTitle = () => {
        if (page == 'home') {
            return 'Mes réglages'
        } else if (page == 'info' || page == 'modif') {
            return 'Mes Informations'
        } else if (page == 'theme') {
            return 'Mon thème'
        }
    }

    const renderAction = () => {
        if (page == 'home') {
            return navigation.goBack()
        } else if (page == 'info' ) {
            return setPage('home')
        } else if (page == 'modif') {
            return setPage('info')
        } else if (page == 'theme') {
            return setPage('home')
        }
    }

    const save = () => {
        if (!firstName || !lastName || !email) {
            Alert.alert(
                'Une erreure est survenue',
                'Tous les champs nont pas été remplis',
                [
                    { text: 'OK'},
                ],
                { cancelable: false }
            );
            return;
        }
        const form = new FormData()
        form.append('firstName', firstName)
        form.append('lastName', lastName)
        form.append('email', email)

        const options = {
            method: 'POST',
            url: `${APP_URL}/api/profil/update`,
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${userToken}`
            },
            data: form
        }
        axios.request(options).then((response) => {
            setUserInfo(JSON.stringify(response.data))
            setPage('info')
        }).catch((error) => {
            console.error(error)
        })
    }

    const rgpdRequest = async () => {
        const options = {
            method: 'GET',
            url: `${APP_URL}/api/profil/data-export`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        axios.request(options).then((response) => {
            if (response.data.message == 'email envoyé') {
                Alert.alert(
                    'Infomations',
                    'Suite à votre demande, vous allez recevoir un email avec l\'ensemble de vos données.',
                    [
                        { text: 'OK'},
                    ],
                    { cancelable: false }
                );
                return;
            }
        }).catch((error) => {
            console.error(error)
        })
    }

    const openSettings = () => {
        if (Platform.OS === 'ios') {
            Linking.openURL('app-settings:')
        } else {
            Linking.openSettings()
        }
    }

    return (
        <SafeAreaView style={[styles.container, {backgroundColor: theme == 'dark' ? 'black' : '#F5F5F5'}]}>
            <ScrollView 
                showsVerticalScrollIndicator={false}
                style={{paddingHorizontal: 15, paddingVertical: 20}}>
                {page == 'home' && (
                    <View style={styles.container}>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]} onPress={() => setPage('info')}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Informations personnelles</Text>
                            <Ionicons name="chevron-forward-outline" color={theme == 'dark' ? 'white' : 'black'} size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]} onPress={() => rgpdRequest()}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Demander mes données</Text>
                            <Ionicons name="chevron-forward-outline" color={theme == 'dark' ? 'white' : 'black'} size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]} onPress={() => setPage('theme')}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Thème</Text>
                            <Ionicons name="chevron-forward-outline" color={theme == 'dark' ? 'white' : 'black'} size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]} onPress={() => openSettings()}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Autorisation</Text>
                            <Ionicons name="chevron-forward-outline" color={theme == 'dark' ? 'white' : 'black'} size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]} className="bg-red-400" onPress={() => {logout()}}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Me déconnecter</Text>
                            <Ionicons name="log-out-outline" color={theme == 'dark' ? 'white' : 'black'} size={25} />
                        </TouchableOpacity>
                    </View>
                )}
                {page == 'info' && (
                    <View style={styles.container}>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]}>
                            <Text style={[styles.boxTitle, {color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Nom</Text>
                            <Text style={{color: '#aaa', fontSize: 18}}>{user.lastName}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]}>
                            <Text style={[styles.boxTitle, {color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Prénom</Text>
                            <Text style={{color: '#aaa', fontSize: 18}}>{user.firstName}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]}>
                            <Text style={[styles.boxTitle, {color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Email</Text>
                            <Text style={{color: '#aaa', fontSize: 18}}>{user.email}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]} onPress={() => setPage('modif')}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Modifier</Text>
                            <Ionicons name="pencil-outline" color={theme == 'dark' ? 'white' : 'black'} size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : '#F5F5F5'}]}>
                            <Text style={[styles.boxTitle, {fontSize: 16,color: '#dc2626'}]}>Suprimer mon compte</Text>
                            <Ionicons name="trash-outline" color='#dc2626' size={25} />
                        </TouchableOpacity>
                    </View>
                )}
                {page == 'modif' && (
                    <View style={styles.container}>
                        <InputField
                            label="Prénom"
                            value={firstName}
                            onChangeText={(text) => {
                                setFirstName(text)
                            }}
                        />
                        <InputField
                            label="Nom"
                            value={lastName}
                            onChangeText={(text) => {
                                setLastName(text)
                            }}
                        />
                        <InputField
                            label="Email"
                            value={email}
                            onChangeText={(text) => {
                                setEmail(text)
                            }}
                        />
                        <TouchableOpacity style={[styles.button, {backgroundColor: theme == 'dark' ? '#343434' : '#F6F6F6'}]} onPress={() => save()}>
                            <Text style={[styles.boxTitle, {fontSize: 18, color: theme == 'dark' ? '#F5F5F5' : 'black'}]}>Enregistrer</Text>
                        </TouchableOpacity>
                </View>
                )}
                {page == 'theme' && (
                    <View style={styles.container}>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : 'white'}]} onPress={() => changeTheme('dark')}>
                            <Text style={{color: theme == 'dark' ? 'white' : 'black'}}>Sombre</Text>
                            <RadioButton value={theme} selected={theme == 'dark'}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : 'white'}]} onPress={() => changeTheme('white')}>
                            <Text style={{color: theme == 'dark' ? 'white' : 'black'}}>Clair</Text>
                            <RadioButton value={theme} selected={theme == 'white'} />
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.box, {backgroundColor: theme == 'dark' ? '#333333' : 'white'}]} onPress={() => changeTheme('system')}>
                            <Text style={{color: theme == 'dark' ? 'white' : 'black'}}>Système</Text>
                            <RadioButton value={theme} selected={theme == 'system'} />
                        </TouchableOpacity>
                    </View>
                )}
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 20
    },
    red: {
        backgroundColor: 'red'
    },
    button: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 8,
      padding: 20,
      marginBottom: 10,
      color: '#ffffff',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.2,
      shadowRadius: 2,
      elevation: 2,
    },
    box: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 8,
      padding: 20,
      marginBottom: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.2,
      shadowRadius: 2,
      elevation: 2,
    },
    boxTitle: {
      fontSize: 12,
    },
    title: {
      fontSize: 25
    }
  })