import React, { useContext, useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ScrollView, TouchableOpacity, useColorScheme, Image } from 'react-native'
import Ionicons from '@expo/vector-icons/Ionicons'
import { useFocusEffect, useNavigation, useRoute } from '@react-navigation/core'
import Box from '../../../components/Box'
import { APP_URL } from '../../../config'
import axios from 'axios'
import { AuthContext } from '../../../context/AuthContext'


export default function UserPage() {

    const {userToken, userInfo} = useContext(AuthContext)
    const [user, setUser] = useState([])
    const navigation = useNavigation()
    const [posts, setPosts] = useState([])
    const route = useRoute()
    const id = route.params.id
    const colorScheme = useColorScheme()

    const getUser = async () => {
        const options = {
        method: 'GET',
        url: `${APP_URL}/api/profil/${id}`,
        headers: {
            Authorization: `Bearer ${userToken}`
        }
        }
        await axios.request(options).then((response) => {
            setUser(response.data)
        }).catch((error) => {
            console.error(error)
        })
    }

    const getPosts = async () => {
      const options = {
        method: 'GET',
        url: `${APP_URL}/api/profil/posts/${id}`,
        headers: {
          Authorization: `Bearer ${userToken}`
        }
      }
      await axios.request(options).then((response) => {
        setPosts(response.data)
      }).catch((error) => {
        console.error(error)
    })
    }

    const handleRefresh = async () => {
      await getPosts()
      await getUser()
    }

    useFocusEffect(
        React.useCallback(() => {
            handleRefresh()
        }, [])
    )

  return (
    <SafeAreaView style={[styles.container, {backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}]}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ paddingVertical: 20}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-start'}}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Ionicons name="chevron-back-outline" color={colorScheme == 'dark' ? '#F5F5F5' : 'black'} size={30} />
            </TouchableOpacity>
            <Text className="ml-2" style={[styles.title, {color: colorScheme == 'dark' ? '#F5F5F5' : 'black'}]}>
              {user.firstName} {user.lastName}
            </Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}} className="mt-5">
            <Image 
              source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ user.firstName + user.lastName +'&backgroundColor[]' }} 
              style={{ width: 75, height: 75, borderRadius: 100, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
              className="border-1"
            />
            <View>
              <Text className="text-center" style={[{color: colorScheme == 'dark' ? 'white' : 'black'}]}>
                {posts.posts && (
                  posts.posts.length
                )}
              </Text>
              <Text style={[{color: colorScheme == 'dark' ? 'white' : 'black'}]}>post</Text>
            </View>
            <View>
              <Text className="text-center" style={[{color: colorScheme == 'dark' ? 'white' : 'black'}]}>
                {posts.keeping && (
                  posts.keeping.length
                )}
              </Text>
              <Text style={[{color: colorScheme == 'dark' ? 'white' : 'black'}]}>gardiennage</Text>
            </View>
            <View>
              <Text className="text-center" style={[{color: colorScheme == 'dark' ? 'white' : 'black'}]}>
                0
              </Text>
              <Text style={[{color: colorScheme == 'dark' ? 'white' : 'black'}]}>avis</Text>
            </View>
          </View>
          {user.roles && user.roles.includes('ROLE_BOTANISTE') && (
            <View className="mt-10 flex-row">
              <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
              <Text style={{color: '#aaa'}}>Cette utilisateur est un botaniste de l'application</Text>
            </View>
          )}
          {id != userInfo.id && (
            <View className="mt-5 flex-1 mx-2">
              <TouchableOpacity className="mx-auto w-full" style={[styles.box, {backgroundColor: colorScheme == 'dark' ? '#333333' : 'white'}]} onPress={() => navigation.navigate('message_detail', {id: user.id, firstName: user.firstName, lastName: user.lastName})}>
                <Text style={[styles.boxTitle, {color: colorScheme == 'dark' ? '#F5F5F5' : 'black'}]}>Envoyer un message</Text>
              </TouchableOpacity>
            </View>
          )}
          <View className="mt-5" style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
            {posts.posts && (
              posts.posts.map((item) => (
                <Box
                  key={item.id}
                  post={item}
                  detail={() => navigation.navigate('post_detail', {id: item.id})}
                  url="/images/post/"
                />
                
              ))
            )}
        </View>
        </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    paddingTop: 20
  },
  box: {
    backgroundColor: '#ffffff',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: '100%',
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2,
  },
  boxTitle: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  title: {
    fontSize: 25
  }
});