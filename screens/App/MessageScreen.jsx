import { StyleSheet, Text, View, SafeAreaView, ScrollView, useColorScheme, TouchableOpacity, Platform } from 'react-native'
import { StatusBar } from 'expo-status-bar'
import React, { useContext, useState } from 'react'
import { AuthContext } from '../../context/AuthContext'
import { APP_URL } from '../../config'
import axios from 'axios';
import { useFocusEffect, useNavigation } from '@react-navigation/native'
import BoxConversation from '../../components/message/BoxConversation'
import InputField from '../../components/InputField'
import Ionicons from '@expo/vector-icons/Ionicons'


export default function MessageScreen() {
    const [conversations, setConversations] = useState([])
    const {userToken} = useContext(AuthContext)
    const navigation = useNavigation()
    const colorScheme = useColorScheme()
    const [filter, setFilter] = useState('')
    const [messageFiltered, setMessageFiltered] = useState(null)

    const getConversations = async () => {
        const options = {
            method: 'GET',
            url: `${APP_URL}/api/message`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        axios.request(options).then((response) => {
            setConversations(response.data)
        }).catch((error) => {
            console.error(error.message)
        })
    }

    const getMessages = (text) => {
        if (text != '') {
            setMessageFiltered(conversations.filter((item) => {
                return item.user.lastName.includes(text) || item.user.firstName.includes(text) 
            }))
        } else {
            setMessageFiltered(null)
        }
        setFilter(text)
    }

    const handleRefresh = async () => {
        await getConversations()
    }

    useFocusEffect(
        React.useCallback(() => {
            handleRefresh()
        }, [])
    )

    return (
        <SafeAreaView style={[styles.container, {paddingTop: Platform.OS == 'android' ? 25 : 0, backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}]}>
            <ScrollView 
                showsVerticalScrollIndicator={false}
                style={{paddingHorizontal: 15, paddingVertical: 20}}
                >
                <View style={[styles.container, {marginBottom: 25, flexDirection: 'row', justifyContent:'space-between', alignItems: 'center'}]}>
                    <Text style={[styles.title, {color: colorScheme == 'dark' ? 'white' : 'black'}]}>
                        Mes messages
                    </Text>
                    <TouchableOpacity>
                        <Ionicons name="create-outline" color={colorScheme == 'dark' ? 'white' : 'black'} size={20}/>
                    </TouchableOpacity>
                </View>
                <View className="w-full px-2 mb-5">
                    <InputField
                        icon={<Ionicons name="search-outline" color="grey" size={20}/>}
                        label="Rechercher"
                        value={filter}
                        onChangeText={(text) => getMessages(text)}
                        keyboardType="web-search"
                    />
                </View>
                {messageFiltered == null && (
                    <View>
                        {conversations.map((item, index) => (
                            <BoxConversation
                                key={index}
                                conversation={item}
                                navigation={navigation}
                                colorScheme={colorScheme}
                            />
                        ))}
                    </View>
                )}
                {messageFiltered != null && (
                    <View>
                        <View>
                            {messageFiltered.map((item, index) => (
                                <BoxConversation
                                    key={index}
                                    conversation={item}
                                    navigation={navigation}
                                    colorScheme={colorScheme}
                                />
                            ))}
                        </View>
                        <View style={{opacity: 0.5}}>
                            {conversations.filter(item => !messageFiltered.includes(item)).map((item, index) => (
                                <BoxConversation
                                    key={index}
                                    conversation={item}
                                    navigation={navigation}
                                    colorScheme={colorScheme}
                                />
                            ))}
                        </View>
                    </View>
                )}
                <StatusBar style="auto" />
            </ScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    title: {
        fontSize: 25
    },
    container: {
        flex:1
    }
});