import React, { useState } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, useColorScheme } from 'react-native'
import { Image } from 'expo-image'
import * as ImagePicker from 'expo-image-picker'
import * as Location from 'expo-location'
import { Camera } from 'expo-camera'
import { useNavigation } from '@react-navigation/core'
import AsyncStorage from '@react-native-async-storage/async-storage'

const AutorisationScreen = () => {
  const colorScheme = useColorScheme();
  const [step, setStep] = useState(1); 
  const navigation = useNavigation();

  const askForPermission = async () => {
    let response;
    try {
      switch (step) {
        case 1: 
          response = await ImagePicker.requestMediaLibraryPermissionsAsync()
          break;
        case 2:
          response = await Location.requestForegroundPermissionsAsync()
          break;
        case 3: 
          response = await Camera.requestCameraPermissionsAsync()
          break;
        default:
          return;
      }
    } catch (err) {
      console.error(err)
    }
    setTimeout(goToNextStep, 0);
  }

  const goToNextStep = async () => {
    if (step < 3) {
      setStep(step + 1);
    } else {
      await AsyncStorage.setItem('autorisationsChecked', 'true')
      navigation.goBack()
    }
  };

  return (
    <SafeAreaView className="w-full h-screen" style={{backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}}>
        <View className="flex-1" style={{backgroundColor: colorScheme === 'dark' ? 'black' : '#F5F5F5'}}>
          {step === 1 && (
            <View>
              <View className="mt-16 items-center px-4">
                <Image
                    source={require('../../../assets/authorisation/galerie_autorisation.svg')}
                    className="w-full h-2/3"
                    contentFit="contain"
                />
              </View>
              <View className="px-8 -mt-8">
                <Text className="text-[#334748] text-center text-2xl">
                    Accordez le <Text className="text-[#66937B] font-semibold">droit</Text> à votre <Text className="text-[#66937B] font-semibold">galerie</Text> afin de pouvoir alimenter les posts
                </Text>
              </View>
            </View>
          )}
          {step === 2 && (
            <View>
              <View className="mt-16 items-center px-4">
                <Image
                    source={require('../../../assets/authorisation/localisation_autorisation.svg')}
                    className="w-full h-2/3"
                    contentFit="contain"
                />
              </View>
              <View className="px-8 -mt-8">
                <Text className="text-[#334748] text-center text-2xl">
                    Accordez le <Text className="text-[#66937B] font-semibold">droit</Text> à votre <Text className="text-[#66937B] font-semibold">localisation</Text> afin que l'on vous proposes les posts <Text className="text-[#66937B] font-semibold">les plus proches</Text> 
                </Text>
              </View>
            </View>
          )}
          { step === 3 && (
            <View>
              <View className="mt-16 items-center px-4">
                <Image
                    source={require('../../../assets/authorisation/photo_autorisation.svg')}
                    className="w-full h-2/3"
                    contentFit="contain"
                />
            </View>
            <View className="px-8 -mt-8">
              <Text className="text-[#334748] text-center text-2xl">
                  Accordez le <Text className="text-[#66937B] font-semibold">droit</Text> à votre <Text className="text-[#66937B] font-semibold">appareil photo</Text> afin de pouvoir alimenter les posts
              </Text>
            </View>
            </View>
          )}
          <View className="-mt-8 mb-8 items-center">
            <TouchableOpacity className="bg-[#66937B] rounded-full px-12 py-3" onPress={() => askForPermission()}>
                <Text className="text-white-light text-lg">Autoriser l'accès</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={goToNextStep}>
                <Text className="mt-4 text-gray-500" style={{color: colorScheme === 'dark' ? '#F5F5F5' : '#333333'}}>Autoriser plus tard</Text>
              </TouchableOpacity>
            </View>
          </View>
    </SafeAreaView>
  );
};

export default AutorisationScreen;
