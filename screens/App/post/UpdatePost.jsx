import React, { useCallback, useContext, useMemo, useRef, useState } from 'react';
import { View, Image, SafeAreaView,TextInput, ScrollView, TouchableOpacity, Text, StyleSheet, KeyboardAvoidingView, Platform, useColorScheme, Alert } from 'react-native';
import { APP_URL } from '../../../config';
import Ionicons from '@expo/vector-icons/Ionicons'
import { AuthContext } from '../../../context/AuthContext';
import { useFocusEffect, useRoute } from '@react-navigation/native';
import axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker'

export default function UpdatePost({ navigation }) {
    const {userToken} = useContext(AuthContext)
    const route = useRoute()
    const id = route.params.id
    const [post, setPost] = useState([])
    const [user, setUser] = useState([])
    const colorScheme = useColorScheme()
    const [isExpanded, setIsExpanded] = useState(false)
    const [namePlant, setNamePlant] = useState(null)
    const [description, setDescription] = useState(null)
    const [city, setCity] = useState(null)
    const [zipcode, setZipcode] = useState(null)
    const [dateStart, setDateStart] = useState(null)
    const [dateEnd, setDateEnd] = useState(null)
    const [showStartDatePicker, setShowStartDatePicker] = useState(false)
    const [showEndDatePicker, setShowEndDatePicker] = useState(false)


    const getPost = async () => {
        const options = {
            method: 'GET',
            url: `${APP_URL}/api/post/${id}`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        await axios.request(options).then((response) => {
            setUser(response.data.postedBy)
            setPost(response.data)
            setNamePlant(response.data.namePlant)
            setCity(response.data.city)
            setZipcode(response.data.zipcode)
            setDescription(response.data.description)
            setDateStart(new Date(response.data.startAt))
            setDateEnd(new Date(response.data.endAt))
        }).catch((error) => {
            console.error(error)
        })
    }

    const handleRefresh = async () => {
        await getPost()
    }

    useFocusEffect(
        React.useCallback(() => {
            handleRefresh()
        }, [])
    )
    const renderImageMain = (images) => {
        const image = images.filter(item => item.isMain === true)
        return image[0]
    }

    const isModif = () => {
        return post.namePlant != namePlant || post.city != city || post.zipcode != zipcode || post.description != description
    }

    const update = () => {
        if (namePlant == '' || city == '' || zipcode == '' || description == '') {
            Alert.alert(
                'Une erreure est survenue',
                'Tous les champs nont pas été remplis',
                [
                    { text: 'OK'},
                ],
                { cancelable: false }
            );
            return;
        }

        const formData = new FormData()
        formData.append("namePlant", namePlant)
        formData.append("description", description)
        formData.append("city", city)
        formData.append("zipcode", zipcode)
        formData.append("startAt", dateStart.toISOString())
        formData.append("endAt", dateEnd.toISOString())

        const options = {
            method: 'POST',
            url: `${APP_URL}/api/post/${post.id}`,
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${userToken}`
            },
            data: formData
        }
        axios.request(options).then((response) => {
            return response.data
        }).catch((error) => {
            console.error(error)
        })
    }

    const handleDelete = () => {
        const options = {
            method: 'DELETE',
            url: `${APP_URL}/api/post/${post.id}`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        axios.request(options).then((response) => {
            if (response.data.message == 'Post supprimé avec succès') {
                navigation.goBack()
            }
        })
    }

    const deletePost = () => {
        Alert.alert(    
            'Suppresion de cette belle plante',
            'Etes vous sur de vouloir la supprimer ? cette action est irréversible',
            [
                {
                  text: 'Non',
                  style: 'cancel',
                },
                {
                  text: 'Oui',
                  onPress: () => {
                    handleDelete()
                  },
                },
              ]
        );
    }

    return (
        <View style={[{flex: 1}, {backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}]}>
            <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            keyboardVerticalOffset={Platform.select({ ios: 0, android: 0 })}
        >
            {post.images && (
                <Image
                    source={{uri: APP_URL + renderImageMain(post.images).name }}
                    resizeMode="cover"
                    style={{position: 'absolute', width: '100%', height: 300}}
                />
            )}
            <View className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute top-10 left-2 z-10">
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Ionicons name="chevron-back-outline" color="white" size={25} />
                </TouchableOpacity>
            </View>
            <View className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute top-10 right-2 z-10">
                <TouchableOpacity onPress={deletePost}>
                    <Ionicons name="trash-outline" color="red" size={25} />
                </TouchableOpacity>
            </View>
            {post.images && isExpanded && (
                    <View className="bg-[#333333]/70 h-screen flex-1 justify-center items-center" style={{position: 'absolute', width: '100%', top: -50, left: 50,zIndex: 999 ,transform:[{translateY: 50},{translateX: -50}]}}>
                        <TouchableOpacity className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute z-10 top-10 right-2" onPress={() => setIsExpanded(false)}>
                            <Ionicons name="close-outline" color="white" size={25} />
                        </TouchableOpacity>
                        <Image
                            source={{uri: APP_URL + renderImageMain(post.images).name }}
                            resizeMode="cover"
                            style={{width: '95%', height: '80%'}}
                            className="rounded-lg"
                        />
                    </View>
            )}
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow: 1, paddingTop: 250}}>
                <View className="h-8 w-8 bg-[#333333]/80 flex items-center justify-center rounded-full ml-auto mr-5">
                    <TouchableOpacity onPress={() => setIsExpanded(true)}>
                        <Ionicons name="expand-outline" color="white" size={20} />
                    </TouchableOpacity>
                </View>
                <View className="flex-1 pt-5 px-2 rounded-t-lg" style={{backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}}>
                    <View className="flex-row justify-center align-center mt-2">
                        <View style={{ flex: 1 }}>
                            <TextInput style={[{ fontWeight: 'bold' }]} className="text-2xl text-vert-v2 bg-[#D3D3D3]/20 rounded-lg p-1"
                                label="Nom de la plante"
                                value={namePlant}
                                onChangeText={setNamePlant}
                            />
                            <View style={{ color: '#aaa'}} className="py-1 flex-row align-center">
                                <Ionicons name="location-sharp" color="#aaa" size={15}/>
                                <TextInput style={[{ fontWeight: 'bold' }]} className="bg-[#D3D3D3]/20 rounded-lg p-1 text-[#fff]"
                                    label="Ville"
                                    value={city}
                                    onChangeText={setCity}
                                />
                                <TextInput style={[{ fontWeight: 'bold' }]} className="bg-[#D3D3D3]/20 rounded-lg p-1 text-[#fff]"
                                    label="code postal"
                                    value={zipcode}
                                    onChangeText={setZipcode}
                                />
                            </View>
                            {dateStart != null && dateEnd != null && (
                                <View className="py-1 flex-column align-center">
                                    <View className="flex-row">
                                        <Ionicons name="calendar-outline" color="#aaa" size={15}/>
                                        <Text style={{ color: '#aaa'}} className="ml-2">
                                            Plage de gardiennage
                                        </Text>    
                                    </View>
                                    <View className="flex-row align-center items-center">
                                        <Text style={{color: colorScheme == 'dark' ? '#F5F5F5' : 'black'}}>Du</Text>
                                        <TouchableOpacity 
                                            onPress={() => setShowStartDatePicker(!showStartDatePicker)}
                                            className="p-2 m-2 rounded-lg"
                                            style={{backgroundColor: colorScheme == 'dark' ? '#333333' : 'white', borderColor: colorScheme == 'dark' ? '#333333' : 'black'}}
                                        >
                                            <Text style={{color: colorScheme === 'dark' ? 'grey' : 'black'}}>{dateStart.toLocaleDateString()}</Text>
                                        </TouchableOpacity>
                                        <Text style={{color: colorScheme == 'dark' ? '#F5F5F5' : 'black'}}>au</Text>
                                        <TouchableOpacity 
                                            onPress={() => setShowEndDatePicker(!showEndDatePicker)}
                                            className="p-2 m-2 rounded-lg"
                                            style={{backgroundColor: colorScheme == 'dark' ? '#333333' : 'white', borderColor: colorScheme == 'dark' ? '#333333' : 'black'}}
                                        >
                                            <Text style={{color: colorScheme === 'dark' ? 'grey' : 'black'}}>{dateEnd.toLocaleDateString()}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}
                            {showStartDatePicker && (
                                <DateTimePicker
                                    value={dateStart}
                                    mode="date"
                                    display={Platform.OS === 'ios' ? 'spinner' : 'default'}
                                    onChange={setDateStart}
                                />
                            )}
                            {showEndDatePicker && (
                                <DateTimePicker
                                    value={dateEnd}
                                    mode="date"
                                    display={Platform.OS === 'ios' ? 'spinner' : 'default'}
                                    onChange={setDateEnd}
                                />
                            )}
                        </View>
                    </View>
                    <View className="mt-5 px-2 text-sm">
                        <Text className="flex-row align-center text-xl" style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                            <Ionicons name="leaf-outline" color="#66937B" size={20}/>
                            Description
                        </Text>
                        <TextInput 
                            className="bg-[#D3D3D3]/20 rounded-lg p-1 text-[#fff] h-36" 
                            style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333', textAlignVertical: 'top'}}
                            label="description"
                            value={description}
                            onChangeText={setDescription}
                            multiline
                        />
                    </View>
                    {isModif() && (
                        <View className="flex-row justify-center mb-20">
                            <TouchableOpacity onPress={update} className="bg-vert-v2 rounded-full p-3 w-full flex-row flex-column mt-4 justify-center">
                                <Text className="text-white text-lg uppercase mr-2">Sauvegarder</Text>
                                <Ionicons name={"share-outline"} color="black" size={24} />
                            </TouchableOpacity>
                        </View>
                    )}
                </View>
            </ScrollView>
            </KeyboardAvoidingView>
        </View>
    )
}

const styles = StyleSheet.create({
    box: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        padding: 15,
        margin: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 2,
    },
    boxTitle: {
        fontSize: 15,
        textAlign: 'center'
    },
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    textInput: {
        flex: 1,
        height: 40,
        borderWidth: 1,
        borderRadius: 20,
        paddingHorizontal: 10,
    },
    sendButton: {
        marginLeft: 10,
        backgroundColor: '#017C01',
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    sendButtonText: {
        color: '#ffffff',
        fontWeight: 'bold',
    },
})