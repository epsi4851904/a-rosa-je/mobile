import React, { useCallback, useContext, useMemo, useRef, useState } from 'react';
import { View, Image, SafeAreaView,TextInput, ScrollView, TouchableOpacity, Text, StyleSheet, KeyboardAvoidingView, Platform, useColorScheme } from 'react-native';
import { APP_URL } from '../../../config';
import Ionicons from '@expo/vector-icons/Ionicons'
import { AuthContext } from '../../../context/AuthContext';
import { useFocusEffect, useRoute } from '@react-navigation/native';
import axios from 'axios';
import BottomSheet from '@gorhom/bottom-sheet'
import moment from 'moment'
import ImagePlant from '../../../components/post/ImagePlant'
import CommentItem from '../../../components/comment/commentItem';

export default function DetailPost({ navigation }) {
    const {userToken, userInfo, setIsLoading} = useContext(AuthContext)
    const route = useRoute()
    const id = route.params.id
    const [post, setPost] = useState([])
    const [user, setUser] = useState([])
    const [comments, setComments] = useState([])
    const userConnected = userInfo
    const bottomSheetRef = useRef(null)
    const snapPoints = useMemo(() => ['85%'], [])
    const [newComment, setNewComment] = useState('')
    const colorScheme = useColorScheme()
    const [isExpanded, setIsExpanded] = useState(false)


    const getPost = async () => {
        const options = {
            method: 'GET',
            url: `${APP_URL}/api/post/${id}`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }
        await axios.request(options).then((response) => {
            setUser(response.data.postedBy)
            setPost(response.data)
            setComments(response.data.comments)
        }).catch((error) => {
            console.error(error)
        })
    }

    const getComments = (images) => {
        return images.filter((item) => {
            return item.comments
        })
    }

    const handleRefresh = async () => {
        await getPost()
    }

    useFocusEffect(
        React.useCallback(() => {
            handleRefresh()
        }, [])
    )
    const renderImageMain = (images) => {
        const image = images.filter(item => item.isMain === true)
        return image[0]
    }

    const keeping = () => {
        const options = {
            method: 'POST',
            url: `${APP_URL}/api/post/${post.id}/keep-suggets`,
            headers: {
                Authorization: `Bearer ${userToken}`
            }
        }

        axios.request(options).then((response) => {
            setPost(response.data)
            handleRefresh()
        }).catch((error) => {
            console.error(error)
        })
    }

    const sendComment = async () => {
        try {
            if (newComment.trim() !== '') {
                const formData = new FormData();
                formData.append('comment', newComment);

                const options = {
                    method: 'POST',
                    url: `${APP_URL}/api/comment/${post.id}`,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        Authorization: `Bearer ${userToken}`
                    },
                    data: formData
                };

                await axios.request(options).then((response) => {
                    setNewComment('')
                    setComments(response.data)
                }).catch((error) => {
                    console.error(error)
                })
            } else {
                Alert.alert('Error', 'Please enter a non-empty comment');
            }
        } catch (error) {
            console.error('Error sending comment:', error);
            Alert.alert('Error', 'Failed to send comment. Please try again later.');
        }
    };

    const handlePresentModalPress = useCallback(() => {
        bottomSheetRef.current?.expand();
    }, []);

    const requestAcceptation = (value) => {
        const form = new FormData()
        form.append('isValidated', value)

        const options = {
            method: 'POST',
            url: `${APP_URL}/api/post/${post.id}/keep`,
            headers: {
                Authorization: `Bearer ${userToken}`,
                'Content-Type': 'multipart/form-data'
            },
            data: form
        }

        axios.request(options).then((response) => {
            handleRefresh()
        }).catch((error) => {
            console.error(error)
        })
    }

    return (
        <View style={[{flex: 1}, {backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}]}>
            {post.images && (
                <Image
                    source={{uri: APP_URL + renderImageMain(post.images).name }}
                    resizeMode="cover"
                    style={{position: 'absolute', width: '100%', height: 300}}
                />
            )}
            <View className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute top-10 left-2 z-10">
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Ionicons name="chevron-back-outline" color="white" size={25} />
                </TouchableOpacity>
            </View>
            {post.images && isExpanded && (
                    <View className="bg-[#333333]/70 h-screen flex-1 justify-center items-center" style={{position: 'absolute', width: '100%', top: -50, left: 50,zIndex: 999 ,transform:[{translateY: 50},{translateX: -50}]}}>
                        <TouchableOpacity className="h-8 w-8 bg-[#333333]/90 flex items-center justify-center rounded-full absolute z-10 top-10 right-2" onPress={() => setIsExpanded(false)}>
                            <Ionicons name="close-outline" color="white" size={25} />
                        </TouchableOpacity>
                        <Image
                            source={{uri: APP_URL + renderImageMain(post.images).name }}
                            resizeMode="cover"
                            style={{width: '95%', height: '80%'}}
                            className="rounded-lg"
                        />
                    </View>
            )}
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow: 1, paddingTop: 250}}>
                <View className="h-8 w-8 bg-[#333333]/80 flex items-center justify-center rounded-full ml-auto mr-5">
                    <TouchableOpacity onPress={() => setIsExpanded(true)}>
                        <Ionicons name="expand-outline" color="white" size={20} />
                    </TouchableOpacity>
                </View>
                <View className="flex-1 pt-5 px-2 rounded-t-lg" style={{backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}}>
                    <TouchableOpacity onPress={() => navigation.navigate('user_detail', {id: user.id})}>
                        <View className="flex-row justify-center align-center mt-2">
                            <View style={{ flex: 1 }}>
                                <Text style={[{ fontWeight: 'bold' }]} className="text-2xl text-vert-v2">
                                    {post.namePlant}
                                </Text>
                                <View style={{ color: '#aaa'}} className="py-1 flex-row align-center">
                                    <Ionicons name="location-sharp" color="#aaa" size={15}/>
                                    <Text style={{ color: '#aaa'}} className="ml-2">
                                        {post.city} {post.zipcode}
                                    </Text>
                                </View>
                                {post.startAt != null && post.endAt != null && (
                                    <View className="py-1 flex-row align-center">
                                        <Ionicons name="calendar-outline" color="#aaa" size={15}/>
                                        <Text style={{ color: '#aaa'}} className="ml-2">
                                            Plage de gardiennage
                                            {'\n Du'} {(moment(post.startAt).format('DD/MM/YYYY'))} au {(moment(post.endAt).format('DD/MM/YYYY'))} 
                                        </Text>
                                    </View>
                                )}
                            </View>
                            <Image 
                                source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ user.firstName + user.lastName +'&backgroundColor[]' }} 
                                style={{ width: 40, height: 40, borderRadius: 25, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
                                className="border-1"
                            />
                        </View>
                    </TouchableOpacity>
                    <View className="mt-5 px-2 text-sm">
                        <Text className="flex-row align-center text-xl" style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                            <Ionicons name="leaf-outline" color="#66937B" size={20}/>
                            Description
                        </Text>
                        <Text className="text-sm" style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                            {post.description}
                        </Text>
                        <TouchableOpacity className="mt-10" onPress={() => handlePresentModalPress()}>
                            <View className="flex-row align-center h-10" >
                                <Ionicons name="chatbox-ellipses-outline" color={colorScheme == 'dark' ? '#F5F5F5' : '#333333'} size={15}/>
                                <Text className="ml-2" style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>
                                    { comments.length } {comments.length > 1 ? 'commentaires' : 'commentaire'}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        {post.startAt == null && post.endAt == null && (
                            <View className="py-1 flex-row align-center">
                                <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
                                <Text style={{ color: '#aaa'}} className="ml-2">
                                    Ce post est à but de conseil seul
                                </Text>
                            </View>
                        )}
                        {post.startAt != null && post.endAt != null && (
                            <View>
                                {post.postedBy && post.keepedBy == null && post.postedBy.id == userConnected.id && (
                                    <View className="mt-5 text-center">
                                        <Text style={{ color: '#aaa'}}>Aucune demande soumise sur votre post</Text>
                                    </View>
                                )}
                                {post.postedBy && post.keepedBy == null && post.postedBy.id != userConnected.id && (
                                    <View>
                                        <View className="py-1 flex-row align-center">
                                            <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
                                            <Text style={{ color: '#aaa'}} className="ml-2">
                                                L'utilisateur souhaite faire garder sa plante, ça vous tente ?
                                            </Text>
                                        </View>
                                        <TouchableOpacity className="bg-vert-v2" style={styles.box} onPress={() => keeping()}>
                                            <Text style={styles.boxTitle}>Me proposer pour garder la plante</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                {post.postedBy && !post.keepingValidate && post.keepedBy && post.postedBy.id == userConnected.id && (
                                    <View className="mb-10 text-center">
                                        <View className="py-1 flex-row align-center">
                                            <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
                                            <Text style={{ color: '#aaa'}} className="ml-2">
                                                {post.keepedBy.firstName} s'est proposé pour garder votre plante.
                                            </Text>
                                        </View>
                                        <TouchableOpacity onPress={() => navigation.navigate('user_detail', {id: post.keepedBy.id})}>
                                            <View className="flex-row align-center mt-5">
                                                <Image 
                                                    source={{ uri: 'https://api.dicebear.com/7.x/thumbs/png?seed='+ post.keepedBy.firstName+post.keepedBy.lastName +'&backgroundColor[]' }} 
                                                    style={{ width: 25, height: 25, borderRadius: 25, marginRight: 10, borderColor: 'grey', borderWidth: 1 }}
                                                />
                                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                                    <Text style={{ fontWeight: 'bold', color: '#aaa' }}>{post.keepedBy.firstName} {post.keepedBy.lastName}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <View className="flex-1 flex-row w-full items-center justify-around mt-5">
                                            <TouchableOpacity style={styles.box} className="bg-[#808080] w-1/3" onPress={() => requestAcceptation('refused')}>
                                                <Text style={styles.boxTitle}>Refuser</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.box} className="bg-vert-v2 w-1/3" onPress={() => requestAcceptation('validated')}>
                                                <Text style={styles.boxTitle}>Accepter</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )}
                                {post.keepedBy && post.keepingValidate && post.keepedBy.id == userConnected.id && moment(post.endAt) > (moment.now()) && (
                                    <View className="mt-5 text-center">
                                        <View className="py-1 flex-row align-center">
                                            <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
                                            <Text style={{ color: '#aaa'}} className="ml-2">
                                                Vous êtes le gardien vous devez prendre en photo l'état de la plante pendant le gardiennage
                                            </Text>
                                        </View>
                                        <TouchableOpacity className="bg-[#808080]" style={styles.box} onPress={() => navigation.navigate('add_image', {id: post.id})}>
                                            <Text style={styles.boxTitle}>Publier une image</Text>
                                            <Ionicons name="add-circle-outline" color='black' size={25} />
                                        </TouchableOpacity>
                                    </View>
                                )}
                                {post.images && (
                                    <View className="text-center">
                                        <Text  style={{ color: '#aaa'}}>Les photos du gardien </Text>
                                        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}>
                                            <View className="w-full flex-row justify-start items-center m-5">
                                                {post.images && (
                                                    post.images.map((item, index) => {
                                                        if (!item.isMain) {
                                                            return <ImagePlant key={index} image={item} post={post} refreshPost={handleRefresh} />
                                                        }
                                                    })
                                                )}
                                            </View>
                                        </ScrollView>
                                    </View>
                                )}
                                {post.postedBy && post.keepedBy && post.postedBy.id != userConnected.id && post.keepedBy.id != userConnected.id && (
                                    <View className="py-1 flex-row align-center">
                                        <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
                                        <Text style={{ color: '#aaa'}} className="ml-2">
                                            Une demande a déjà été faite sur ce post.
                                        </Text>
                                    </View>
                                )}
                                {post.postedBy && !post.keepingValidate && post.keepedBy && post.keepedBy.id == userConnected.id && (
                                    <View className="py-1 flex-row align-center">
                                        <Ionicons name="information-circle-outline" color="#aaa" size={15}/>
                                        <Text style={{ color: '#aaa'}} className="ml-2">
                                            Votre demande est en cours de demande de la part du propriétaire.
                                        </Text>
                                    </View>
                                )}
                            </View>
                        )}
                    </View>
                </View>
            </ScrollView>
            <BottomSheet
                ref={bottomSheetRef}
                index={-1}
                snapPoints={snapPoints}
                enablePanDownToClose
                backgroundStyle={{backgroundColor: colorScheme == 'dark' ? 'black' : '#F5F5F5'}}
                handleIndicatorStyle={{backgroundColor: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}
            >
                <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    keyboardVerticalOffset={125}
                >
                    <ScrollView>
                        <View className="flex m-5">
                            <Text style={{color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}}>Commentaires des botanistes</Text>
                            <View>
                                {comments.map((item, index) => (
                                    <CommentItem
                                    key={index}
                                    comment={item}
                                    userToken={userToken}
                                    refreshPost={handleRefresh}
                                    />
                                ))}
                            </View>
                        </View>
                    </ScrollView>
                    {userConnected.roles.includes('ROLE_BOTANISTE') && (
                        <View style={styles.inputContainer}>
                            <TextInput
                                multiline={true}
                                style={[styles.textInput, {borderColor: colorScheme == 'dark' ? '#808080' : '#333333', color: colorScheme == 'dark' ? '#F5F5F5' : '#333333'}]}
                                placeholder="Entrez votre commentaire"
                                placeholderTextColor={colorScheme == 'dark' ? '#F5F5F5' : '#333333'}
                                value={newComment}
                                onChangeText={text => setNewComment(text)}
                            />
                            {newComment.trim() !== '' && (
                                <TouchableOpacity style={styles.sendButton} onPress={sendComment}>
                                    <Ionicons name="paper-plane-outline" color='white' size={20} />
                                </TouchableOpacity>
                            )}
                        </View>
                    )}
                </KeyboardAvoidingView>
            </BottomSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    box: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        padding: 15,
        margin: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 2,
    },
    boxTitle: {
        fontSize: 15,
        textAlign: 'center'
    },
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    textInput: {
        flex: 1,
        height: 40,
        borderWidth: 1,
        borderRadius: 20,
        paddingHorizontal: 10,
    },
    sendButton: {
        marginLeft: 10,
        backgroundColor: '#017C01',
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    sendButtonText: {
        color: '#ffffff',
        fontWeight: 'bold',
    },
})