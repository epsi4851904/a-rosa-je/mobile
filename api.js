import axios from 'axios';
import { APP_URL } from './config.js'

const requestAuth = (method, url, data) => {
    const options = {
        method: method,
        url: `${APP_URL}/api${url}`,
        headers: {'Content-Type': 'application/json'},
        data: data
    }
    return axios.request(options).then((response) => response.data)
}

const requestApi = (method, url, data, token) => {
    const options = {
        method: method,
        url: `${APP_URL}/api${url}`,
        headers: {
            Authorization: `Bearer ${token}`
        },
        data: data
    }
    return axios.request(options).then((response) => response.data)
}


const api = {
    login(email, password) {
        return requestAuth('POST', '/login_check', {
            username: email,
            password: password
        }).then((response) => {
            return response
        }).catch((error) => {
            return error
        });
    },
    refreshToken(tokenRefresh) {
        const options = {
            method: 'POST',
            url: `${APP_URL}/api/token/refresh`,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: `refresh_token=${tokenRefresh}`
        }
        return axios.request(options).then((response) => {
            return response.data
        }).catch((error) => {
            console.error(error);
        })
    },
    register(firstName, lastName, email, password) {
        return requestAuth('POST', '/register', {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password
        }).then((response) => {
            return response
        }).catch((error) => {
            return error
        });
    },
    verifCode(code, token) {
        return requestApi('POST', '/verif-code', {
            code: code
        }, token).then((response) => {
            return response
        }).catch((error) => {
            console.error(error.config)
            console.error(error)
            return error
        })
    }
}

export default api;