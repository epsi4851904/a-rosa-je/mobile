import React from 'react';
import { AuthProvider } from './context/AuthContext';
import { AppNav } from './navigation/AppNav';
import { Platform } from 'react-native';


export default function App() {
    return (
        <AuthProvider>
            <AppNav />
        </AuthProvider>
    )
}