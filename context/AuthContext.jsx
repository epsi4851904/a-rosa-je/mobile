import AsyncStorage from '@react-native-async-storage/async-storage'
import React, { createContext, useEffect, useState} from 'react'
import api from '../api.js'
import { useColorScheme } from 'react-native'

export const AuthContext = createContext()

export const AuthProvider = ({ children }) => {
    const [isLoading, setIsLoading] = useState(false)
    const [isValid, setIsValid] = useState(null)
    const [userToken, setUserToken] = useState(null)
    const [userRefreshToken, setUserRefreshToken] = useState(null)
    const [userInfo, setUserInfo] = useState(null)
    const [username, setUsername] = useState(null)
    const [password, setPassword] = useState(null)
    const [theme, setTheme] = useState(null);


    const login = async (username, password) => {
        setIsLoading(true)
        api.login(username, password).then((response) => {
            setUserToken(response.token)
            AsyncStorage.setItem('userToken', response.token)

            setUserInfo(response.profile)
            AsyncStorage.setItem('userInfo', JSON.stringify(response.profile))

            setUserRefreshToken(response.refresh_token)
            AsyncStorage.setItem('userRefreshToken', response.refresh_token)

            AsyncStorage.setItem('expirationToken', JSON.stringify(response.expiration_date))
            setIsLoading(false)
        })        
    }

    const logout = async () => {
        setIsLoading(true)
        await setUserToken(null)
        await setUserInfo(null)
        AsyncStorage.removeItem('userToken')
        AsyncStorage.removeItem('userInfo')
        AsyncStorage.removeItem('expirationToken')
        AsyncStorage.removeItem('userRefreshToken')
        setIsLoading(false)
    }

    const isLoggedIn = async() => {
        if (userInfo == null) {
            try{
                setIsLoading(true)
                let userToken = await AsyncStorage.getItem('userToken')
                let userRefreshToken = await AsyncStorage.getItem('userRefreshToken')
                let userInfo = await AsyncStorage.getItem('userInfo')
                let expiration_date = await AsyncStorage.getItem('expirationToken')
                let currentDate = new Date()
                let timestamp = currentDate.getTime()
                if (userToken && userInfo && timestamp >= (expiration_date * 1000)){
                    api.refreshToken(userRefreshToken).then((response) => {
                        setUserToken(response.token)
                        AsyncStorage.setItem('userToken', response.token)
                        setUserRefreshToken(response.refresh_token)
                        AsyncStorage.setItem('userRefreshToken', response.refresh_token)
                        AsyncStorage.setItem('expirationToken', JSON.stringify(response.expiration_date))
                    })
                }
                setUserToken(userToken)
                setUserInfo(JSON.parse(userInfo))
                setIsLoading(false)
            } catch(error) {
                return error
            }
        }
    }

    const getTheme = async () => {
        try{
            let theme = await AsyncStorage.getItem('theme')
            if (theme) {
                if (theme == 'dark') {
                    setTheme('dark')
                    return 'dark'
                } else if (theme == 'white') {
                    setTheme('white')
                    return 'white'
                } else {
                    let colorScheme = useColorScheme()
                    setTheme(colorScheme)
                    return useColorScheme()
                }
            }
            const colorScheme = useColorScheme()
            return colorScheme
        }catch(error) {
            console.error(error)
        }
        
    }
    
    const changeTheme = async (theme) => {
        setTheme(theme)
        AsyncStorage.setItem('theme', theme)
    }

    useEffect(() => {
        isLoggedIn()
    }, [])

    return (
        <AuthContext.Provider value={{changeTheme, theme, getTheme, login, logout, isLoading, isValid, username, password, setUsername, setPassword, userToken, userInfo, setUserToken, setUserInfo, setIsLoading, setIsValid, isLoggedIn}}>
            { children }
        </AuthContext.Provider>
    )
}